//
//  UIViewExtension.swift
//  InstaAlerts
//
//  Created by ADMIN on 18/06/19.
//  Copyright © 2019 Rahul Patel. All rights reserved.
//

import UIKit

//CompletionBlock
typealias CompletionBlock = (() -> ())


//MARK: Animated related
extension UIView
{
    /*
     withType: It’s an array of AnimationType to be performed on block.
     reversed: Initial state of the animation. Reverse will start from its original position.
     initialAlpha: Initial alpha of the view prior to the animation.
     finalAlpha: View’s alpha after the animation.
     delay: Time Delay before the animation.
     duration: TimeInterval animation will take to complete.
     animationInterval: TimeInterval between each of the subviews animations.
     backToOriginalForm: View will restore to its identity.
     completion: CompletionBlock after the animation finishes.
     */
    func animate(withType: [AnimationType], reversed: Bool = false, initialAlpha: CGFloat = 0.0, finalAlpha: CGFloat = 1.0, delay: Double = 0.0, duration: TimeInterval = AnimationConfiguration.duration, backToOriginalForm: Bool = false, completion: CompletionBlock? = nil) {
        
        let transformFrom = transform
        var transformTo = transform
        
        withType.forEach { (viewTransform) in
            transformTo = transformTo.concatenating(viewTransform.initialTransform)
        }
        
        if reversed == false {
            transform = transformTo
        }
        
        alpha = initialAlpha
        
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            UIView.animate(withDuration: duration, delay: delay, options: [.curveLinear, .curveEaseInOut], animations: { [weak self] in
                self?.transform = reversed == true ? transformTo : transformFrom
                self?.alpha = finalAlpha
                }, completion: { (_) in
                    completion?()
                    if backToOriginalForm == true {
                        UIView.animate(withDuration: 0.35, delay: 0.0, options: [.curveLinear, .curveEaseInOut], animations: { [weak self] in
                            self?.transform = .identity
                            }, completion: nil)
                    }
            })
        }
    }
    
    /*
     animateAll method require following parameters:
     – withType: Type of animations.
     – interval: Interval time of the animation between subviews.*/
    
    //MARK: - For animate all
    func animateAll(withType: [AnimationType], interval: Double = AnimationConfiguration.interval) {
        for(index, value) in subviews.enumerated() {
            let delay = Double(index) * interval
            value.animate(withType: withType, delay: delay)
        }
    }
    
    /*
     AnimationRandom method require only one parameter:
     – interval: Interval time of the animation between subviews.*/
    
    func animationRandom(interval: Double = AnimationConfiguration.interval) {
        for(index, value) in subviews.enumerated() {
            let delay = Double(index) * interval
            let animationRandom = AnimationType.random()
            value.animate(withType: [animationRandom], delay: delay)
        }
    }
    
    //It will restore all subview to it's identity
    func restoreAllViewToIdentity() {
        for(_, value) in subviews.enumerated() {
            value.transform = CGAffineTransform.identity
        }
    }
}
