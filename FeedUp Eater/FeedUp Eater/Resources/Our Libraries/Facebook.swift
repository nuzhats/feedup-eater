//
//  Facebook.swift
//

import Foundation
import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import SwiftyJSON

struct fbVariable {
    var id : String = ""
    var firstName : String = ""
    var lastName : String = ""
    var userName : String = ""
    var gender : String = ""
    var email : String = ""
    var profic_pic : String = ""
    var birthday : String = ""
    var msg : String = ""
    var status : Int = 0
    var idToken: String = ""
}

struct friendhsVariable {
    var id = [String]()
    var firstName = [String]()
    var lastName = [String]()
    var userName = [String]()
    var gender = [String]()
    var profic_pic = [String]()
    var msg : String? = ""
}



enum MyErrors: Int, Error {
    case Unknown = 404
    
    var localizedDescription: String {
        switch self {
        case .Unknown:
            return NSLocalizedString("\(self.localizedDescription)", tableName: String(describing: self), bundle: Bundle.main, value: "Cancel Button Tap", comment: "")
        }
    }
    var _code: Int { return self.rawValue }
}
public class Facebook : NSObject{
    
    
    typealias Completion = (_ response:friendhsVariable) -> Void
    typealias CompletionHandler = (_ response:fbVariable) -> Void
    typealias ErrorHandler = (_ error : Error?) -> Void
    
    //MARK: Facebook Login
    
    class func configure(application:UIApplication,launchOptions:[UIApplication.LaunchOptionsKey: Any]?)  {
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    /**
     Facbook Log in using FBSDKLoginKit.
     
     :Use it Simply call:
     
     Facebook.LogInWithFacebook(viewController: self, handler: { (response) in
     print(response.email)
     }) { (err) in
     print(err.localizedDescription)
     }
     
     - parameter viewController: assign self viewcontroller
     - parameter handler: handle the response.
     - parameter errorhandler: handle the error. `nil` by default.
     */
    
    class func LogInWithFacebook(viewController : UIViewController,handler:@escaping CompletionHandler,errorhandler : @escaping ErrorHandler)  {
        
        var data = fbVariable()
        
        if (AccessToken.current != nil)
        {
            let parameters = ["fields" : "id, name, email, picture, gender, first_name, last_name, birthday"]
            let request = GraphRequest(graphPath: "me", parameters: parameters)
            
            _ = request.start(completionHandler: { (conn, result, error) in
                if (error != nil) {
                    // Process error
                    errorhandler(error!)
                }
                else
                {
                    // Handle the result
                    
                    data.idToken = AccessToken.current?.tokenString ?? ""
                    if let res = result as? NSDictionary
                    {
                        if let _email = res["email"] as? String{
                            data.email = _email
                        }
                        
                        if let _last_name = res["last_name"] as? String{
                            data.lastName = _last_name
                        }
                        
                        if let _gender = res["gender"] as? String{
                            data.gender = _gender
                        }
                        
                        if let _name = res["name"] as? String{
                            data.userName = _name
                        }
                        
                        if let _id = res["id"] as? String{
                            data.id = _id
                        }
                        
                        if let _birthday = res["birthday"] as? String{
                            data.birthday = _birthday
                        }
                        
                        let imgUrl = ((res.value(forKey: "picture") as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "url") as! String
                        data.profic_pic = imgUrl
                        
                        handler(data)
                    }
                }
            })
        }
        else
        {
            let login = LoginManager()
            //login. = .native
            let permission : Array = ["public_profile", "email"] //,"user_birthday", "user_friends" //commented for YMeet
            
            login.logIn(permissions: permission, from: viewController) { (result, error) in
                
                if (error != nil) {
                    // Process error
                    errorhandler(error!)
                }
                else if (result?.isCancelled)! {
                    // Handle cancelled
                    errorhandler(nil)
                }
                else{
                    // Handle the result
                    if (result?.grantedPermissions.contains("email"))! {
                        
                        let parameters = ["fields" : "id, name, email, picture, gender, first_name, last_name, birthday"]
                        let request = GraphRequest(graphPath: "me", parameters: parameters)
                        
                        _ = request.start(completionHandler: { (conn, result, error) in
                            if (error != nil) {
                                
                            }
                            else
                            {
                                data.idToken = AccessToken.current?.tokenString ?? ""
                                if let res = result as? NSDictionary
                                {
                                    if let _email = res["email"] as? String{
                                        data.email = _email
                                    }
                                    
                                    if let _last_name = res["last_name"] as? String{
                                        data.lastName = _last_name
                                    }
                                    
                                    if let _gender = res["gender"] as? String{
                                        data.gender = _gender
                                    }
                                    
                                    if let _name = res["name"] as? String{
                                        data.userName = _name
                                    }
                                    
                                    if let _id = res["id"] as? String{
                                        data.id = _id
                                    }
                                    
                                    if let _birthday = res["birthday"] as? String{
                                        data.birthday = _birthday
                                    }
                                    
                                    let imgUrl = ((res.value(forKey: "picture") as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "url") as! String
                                    data.profic_pic = imgUrl
                                    
                                    handler(data)
                                }
                            }
                        })
                    }
                    else
                    {
                        // Process error
                        errorhandler(error!)
                    }
                }
            }
        }
    }
    
    /**
     Facbook Logout using FBSDKLoginKit.
     
     :Use it Simply call:
     
     Facebook.LogoutFacebook { (response) in
     print(response.msg)
     }
     
     - parameter handler: handle the response.
     */
    
    class func LogoutFacebook(handler:@escaping CompletionHandler)  {
        
        var data = fbVariable()
        let manager = LoginManager()
        
        if (AccessToken.current != nil) {
            // Handle the result
            data.msg = "Logout Successfully."
            data.status = 1
            handler(data)
            
            manager.logOut()
        }
        else
        {
            // Handle the result
            data.status = 0
            data.msg = "Not Login."
            handler(data)
        }
    }
    
    /**
     Got those facebook friends who installed your application.
     
     :Use it Simply call:
     
     Facebook.GetFacebookFriendsInstallTheApp(viewController: self, handler: { (response) in
     
     if response.id.count != 0
     {
     // got response of username,email in array
     }
     
     }) { (err) in
     print(err.localizedDescription)
     }
     
     - parameter viewController: assign self viewcontroller
     - parameter handler: handle the response.
     - parameter errorhandler: handle the error. `nil` by default.
     */
    
    class func GetFacebookFriendsInstallTheApp(viewController : UIViewController,handler:@escaping Completion,errorhandler : @escaping ErrorHandler)  {
        
        var data = friendhsVariable()
        
        if (AccessToken.current != nil)
        {
            let parameters = ["fields" : "id, name, email, picture, gender, first_name, last_name"]
            let request = GraphRequest(graphPath: "me/friends", parameters: parameters)
            
            _ = request.start(completionHandler: { (conn, result, error) in
                if (error != nil) {
                    // Process error
                    errorhandler(error!)
                }
                else
                {
                    // Handle the result
                    let json = JSON(result!)
                    let datas = json["data"]
                    
                    for (index,_) : (String,JSON) in datas
                    {
                        let name = datas[Int(index)!]["name"].stringValue
                        let first_name = datas[Int(index)!]["first_name"].stringValue
                        let last_name = datas[Int(index)!]["last_name"].stringValue
                        let id = datas[Int(index)!]["id"].stringValue
                        let gender = datas[Int(index)!]["gender"].stringValue
                        let url = datas[Int(index)!]["picture"]["data"]["url"].stringValue
                        
                        data.userName.append(name)
                        data.firstName.append(first_name)
                        data.lastName.append(last_name)
                        data.id.append(id)
                        data.gender.append(gender)
                        data.profic_pic.append(url)
                    }
                    
                    handler(data)
                }
            })
        }
        else
        {
            let login = LoginManager()
            //login.loginBehavior = .native
            let permission : Array = ["public_profile", "email"]
            
            login.logIn(permissions: permission, from: viewController) { (result, error) in
                
                if (error != nil) {
                    // Process error
                    errorhandler(error!)
                }
                else if (result?.isCancelled)! {
                    // Handle cancellations
                    errorhandler(nil)
                }
                else{
                    if (result?.grantedPermissions.contains("email"))! {
                        
                        let parameters = ["fields" : "id, name, email, picture, gender, first_name, last_name"]
                        let request = GraphRequest(graphPath: "me/friends", parameters: parameters)
                        
                        _ = request.start(completionHandler: { (conn, result, error) in
                            if (error != nil) {
                                // Process error
                                errorhandler(error!)
                            }
                            else
                            {
                                // Handle the result
                                let json = JSON(result!)
                                let datas = json["data"]
                                print(datas)
                                
                                for (index,_) : (String,JSON) in datas
                                {
                                    let name = datas[Int(index)!]["name"].stringValue
                                    let first_name = datas[Int(index)!]["first_name"].stringValue
                                    let last_name = datas[Int(index)!]["last_name"].stringValue
                                    let id = datas[Int(index)!]["id"].stringValue
                                    let gender = datas[Int(index)!]["gender"].stringValue
                                    let url = datas[Int(index)!]["picture"]["data"]["url"].stringValue
                                    
                                    data.userName.append(name)
                                    data.firstName.append(first_name)
                                    data.lastName.append(last_name)
                                    data.id.append(id)
                                    data.gender.append(gender)
                                    data.profic_pic.append(url)
                                }
                                
                                handler(data)
                                
                            }
                        })
                    }
                    else
                    {
                        errorhandler(error!)
                    }
                }
            }
        }
    }
    
    /**
     Invite friends to install app using Default image
     
     :Use it Simply call:
     
     Facebook.showAppInviteDialogWithDefaultImage(viewController: self, Applink: appLink, handler: { (response) in
     print(response.msg!)
     }) { (err) in
     print(err.localizedDescription)
     }
     
     - parameter viewController: assign self viewcontroller
     - parameter Applink: Unique app id ex:- https://fb.me/1539184863038815.
     - parameter handler: handle the response.
     - parameter errorhandler: handle the error. `nil` by default.
     */
    
   /* class func showAppInviteDialogWithDefaultImage(viewController : UIViewController,Applink : String,handler:@escaping CompletionHandler,errorhandler : @escaping ErrorHandler) {
        do {
            let appInvite = AppInvite(appLink: URL(string: Applink)!, deliveryMethod: .facebook)
            try AppInvite.Dialog.show(from: viewController, invite: appInvite) { result in
                switch result {
                case .success(_):
                    // Handle the result
                    var data = fbVariable()
                    data.msg = "App Invite Sent Successfully."
                    handler(data)
                case .failed(let error):
                    // Process error
                    errorhandler(error)
                }
            }
        } catch let error {
            // Process error
            errorhandler(error)
        }
    }
    
    /**
     Invite friends to install app using Custom Image
     :Use it Simply call:
     
     Facebook.showAppInviteDialogWithDefaultImage(viewController: self, Applink: appLink, handler: { (response) in
     print(response.msg!)
     }) { (err) in
     print(err.localizedDescription)
     }
     
     - parameter viewController: assign self viewcontroller
     - parameter Applink: Unique app id ex:- https://fb.me/1539184863038815.
     - parameter previewImageURL: local or remote url of image
     - parameter handler: handle the response.
     - parameter errorhandler: handle the error. `nil` by default.
     */
    
    class func showAppInviteDialogWithCustomImage(viewController : UIViewController,Applink : String,previewImageURL : String,handler:@escaping CompletionHandler,errorhandler : @escaping ErrorHandler) {
        do {
            let appInvite = AppInvite(appLink: URL(string: "https://fb.me/1539184863038815")!,
                                      deliveryMethod: .facebook,
                                      previewImageURL: URL(string: previewImageURL))
            try AppInvite.Dialog.show(from: viewController, invite: appInvite) { result in
                switch result {
                case .success(_):
                    // Handle the result
                    var data = fbVariable()
                    data.msg = "App Invite Sent Successfully."
                    handler(data)
                case .failed(let error):
                    // Process error
                    errorhandler(error)
                }
            }
        } catch let error {
            // Process error
            errorhandler(error)
        }
    }
    
    //MARK: Share Method
    
    private class func share<C: ContentProtocol>(_ content: C,handler:@escaping CompletionHandler,errorhandler : @escaping ErrorHandler,view : UIViewController) {
        
        if (FBSDKAccessToken.current() != nil)
        {
            if FBSDKAccessToken.current().hasGranted("publish_actions")
            {
                do {
                    try GraphSharer.share(content) { result in
                        switch result {
                        // Handle the result
                        case .success(let contentResult):
                            print(contentResult)
                            handler(contentResult as! fbVariable)
                        case .cancelled:
                            // Handle cancelled
                            errorhandler(MyErrors.Unknown)
                        case .failed(let error):
                            // Process error
                            errorhandler(error)
                        }
                    }
                } catch (let error) {
                    // Process error
                    errorhandler(error)
                }
            }
            else
            {
                let login = FBSDKLoginManager()
                login.loginBehavior = .native
                
                login.logIn(withPublishPermissions: ["publish_actions"], from: view, handler: { (result, error) in
                    
                    if error != nil
                    {
                        // Process error
                        errorhandler(error!)
                    }
                    else
                    {
                        do {
                            try GraphSharer.share(content) { result in
                                switch result {
                                // Handle the result
                                case .success(let contentResult):
                                    print(contentResult)
                                    handler(contentResult as! fbVariable)
                                case .cancelled:
                                    errorhandler(MyErrors.Unknown)
                                case .failed(let error):
                                    errorhandler(error)
                                }
                            }
                        } catch (let error) {
                            // Process error
                            errorhandler(error)
                        }
                    }
                })
            }
        }
        else
        {
            let login = FBSDKLoginManager()
            login.loginBehavior = .native
            
            login.logIn(withPublishPermissions: ["publish_actions"], from: view, handler: { (result, error) in
                
                if error != nil
                {
                    // Process error
                    errorhandler(error!)
                }
                else
                {
                    do {
                        try GraphSharer.share(content) { result in
                            switch result {
                            case .success(let contentResult):
                                // Handle the result
                                print(contentResult)
                                handler(contentResult as! fbVariable)
                            case .cancelled:
                                // Handle cancelled
                                errorhandler(MyErrors.Unknown)
                            case .failed(let error):
                                // Process error
                                errorhandler(error)
                            }
                        }
                    } catch (let error) {
                        // Process error
                        errorhandler(error)
                    }
                }
            })
        }
    }
    
    /**
     Share on facebook using FBSDKShareKit
     :Use it Simply call:
     
     Facebook.ShareLinkOnFacebook(viewController: self, title: "hello", description: "", url:"https://www.google.com",imageURL: "") { (err) in
     print(err.localizedDescription)
     }
     
     - parameter viewController: assign self viewcontroller
     - parameter title: title is optional
     - parameter description: description is optional
     - parameter url: url is Required.
     - parameter imageURL: url of image is optional.
     - parameter errorhandler: handle the error. `nil` by default.
     */
    
    class func ShareLinkOnFacebook(viewController : UIViewController,title : String?,description : String?,url : String?,imageURL : String?,handler:@escaping CompletionHandler,errorhandler : @escaping ErrorHandler)  {
        let content = LinkShareContent(url: URL(string: url!),
                                       title: title,
                                       description: description,
                                       imageURL: URL(string: imageURL!))
        share(content, handler: handler, errorhandler: errorhandler, view: viewController)
    }*/
    
}


