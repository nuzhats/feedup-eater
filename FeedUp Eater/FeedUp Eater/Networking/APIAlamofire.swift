//
//  APIAlamofire.swift
//  FeedUp Eater
//
//  Created by Jayesh Godhaniya on 21/09/21.
//

import Foundation
import Firebase
import Alamofire

enum Enviroment: String
{
    case development = ""
}

let apiEnviroment :Enviroment = Enviroment.development

let baseURL :String = apiEnviroment.rawValue

var header:[String: String] = [:]
//Store Data for Session Expire
var params:[String: Any] = [:]
var headerForSession:[String: String] = [:]
var sessionImageCheck:[String: UIImage] = [:]
var AlamofiremethodSession : [String: Any] = [:]

typealias reachebility = (_ isReachable :Bool) -> Void

enum APIAction: String
{
    case GetToken = "/getTokan"
    case Login = " "//"/Login"
    case Registration = "/Registration"
    case Forgotpassword = "/ForgotPassword"
    case CompleteEditProfile = "/EditProfile"
    case ChangePassword = "/ChangePassword"
    case Home = "http://www.mocky.io/v2/5d1dbbdb3000003745d7212f"
    case MonthWiseEvent = "GetMonthBaseEventCount"
    case DateWiseEvent = "GetDateBaseEventList"
    case Scan = "http://scanner.crmtiger.com/"//"http://192.168.137.185:8080/file-upload"
}

class AlamofireResponse
{
    var Object: AnyObject
    var code: Int = 0
    var message :String = ""
    
    init(object :AnyObject, code: Int, message: String) {
        
        self.Object = object
        self.code = code
        self.message = message
    }
}

class AlamofireModel: NSObject
{
    
    private static var manager: Alamofire.SessionManager = {

        // Create the server trust policies
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "test.example.com": .disableEvaluation
        ]

        // Create custom manager
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        let manager = Alamofire.SessionManager(
            configuration: URLSessionConfiguration.default,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )

        return manager
    }()
    
    typealias CompletionHandler = (_ response :AlamofireResponse) -> Void
    typealias ErrorHandler = (_ error: Error) -> Void
    
    class func Alamofiremethod(_ method: Alamofire.HTTPMethod, parameter: [String: Any], Header: [String: String], retryCount :Int = 3,isFromLogin:Bool = false, isSupportURL: Bool = false, handler: @escaping CompletionHandler, errorHandler: @escaping ErrorHandler)
    {
        var csvString = ""
        var param : [String:Any] = [String:Any]()
        var URLFinal = ""
        var header = Header
        header["Accept"] = "application/json"
        
        print(header.description)
        
        let rechabilityManager = Alamofire.NetworkReachabilityManager(host: "apple.com")
        
        rechabilityManager?.startListening()
        
        if let r = rechabilityManager
        {
            switch r.isReachable
            {
                
            case true:
                print("reachable")
                
                let alamofiremanager : Alamofire.SessionManager?
                
                
                
                //MARK: - Commented by nuzhat 23/10/2020
//                if !isWordpressAPI
//                {
//                    CheckExpiryDate()
//                }
                
                alamofiremanager = Alamofire.SessionManager.default
                alamofiremanager?.session.configuration.timeoutIntervalForRequest = 31
                alamofiremanager?.session.configuration.timeoutIntervalForResource = 31
                let challengeHandler: ((URLSession, URLAuthenticationChallenge) -> (URLSession.AuthChallengeDisposition, URLCredential?))? = { result, challenge in
                    return (.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
                }
                alamofiremanager?.delegate.sessionDidReceiveChallenge = challengeHandler
                
                //var _parameter = parameter
               
                print(URLFinal)
                print(param)
                
                alamofiremanager?.request(URLFinal, method: method, parameters: param, encoding: URLEncoding.default, headers: header).responseJSON(queue: nil, options: JSONSerialization.ReadingOptions.allowFragments, completionHandler: { (response) in
                    
                    if response.result.isSuccess
                    {
                        if let val = response.result.value
                        {
                            var message = ""
                            var code = response.response?.statusCode ?? 200
                            var responseData: AnyObject = val as AnyObject
                            print(responseData)
                            //For CSV File Error response added by jayesh

                            if let _success = (val as AnyObject).value(forKey: "success") as? Int
                            {
                                code = _success
                                if let _code = (val as AnyObject).value(forKey: "code") as? Int
                                {
                                    if _code == 401//Unauthorize
                                    {
                                        //redirect to login screen
                                        AppInstance.gotoLoginScreen(transition: true)
                                        return
                                        
                                    }
                                }
                                
                                if (val as AnyObject).value(forKey: "result") != nil
                                {
                                    if let _data = (val as AnyObject).value(forKey: "result") as? AnyObject
                                    {
                                        responseData = _data as AnyObject
                                        print(responseData)
                                    }
                                }
                                else
                                {
                                    if let _data = (val as AnyObject).value(forKey: "error") as? AnyObject
                                    {
                                        responseData = _data as AnyObject
                                        print(responseData)
                                    }
                                }
                                
                                
                                
                                if _success == 1
                                {
                                    if let msg = responseData.value(forKey: "message") as? String
                                    {
                                        message = msg
                                    }
                                }
                                else
                                {
                                    
                                        
                                        if let _error =  (val as AnyObject).value(forKey: "error") as? [String]
                                        {
                                            if _error.count > 0
                                            {
                                                message = _error[0]
                                            }
                                        }else if let _error =  (val as AnyObject).value(forKey: "error") as? NSDictionary{
                                            let errorCode = _error.value(forKey: "code")as? Int
                                            if errorCode == 1501{

                                                let tempParam = parameter
                                                //loginUserApiCall()
                                                
                                                DispatchQueue.main.asyncAfter(deadline: .now()+2.0, execute: {
                                                    Alamofiremethod(method, parameter: tempParam, Header: Header, retryCount: retryCount-1, isFromLogin: isFromLogin,isSupportURL: isSupportURL, handler: handler, errorHandler: errorHandler)
                                                })

                                            }else if errorCode == 1555{
                                                
                                                //kCurrentUser.logOut()
                                                //kCurrentUser.saveToDefault()
                                                AppInstance.gotoLoginScreen(transition: true)
                                                showMessage(message: _error.value(forKey: "message")as? String ?? "")
                                                return
                                                
                                            }else if errorCode == 1556
                                            {
                                               // APILogout(isAutoLogin: true)
                                            }else if errorCode == 404{
                                                HideIndicator()
                                                message = _error.value(forKey: "message") as? String ?? ""
                                            }else if errorCode == 403{
                                                //module is disabled
                                                HideIndicator()
                                                message = _error.value(forKey: "message") as? String ?? ""
                                            }
                                            else if errorCode == 1210{//For username password wrong
//                                                kCurrentUser.logOut()
//                                                kCurrentUser.saveToDefault()
//
//                                                kWordPress.logOut()
//                                                kWordPress.saveToDefault()
                                                message = _error.value(forKey: "message") as? String ?? ""
                                                //showMessage(message: _error.value(forKey: "message")as? String ?? "")
//                                                AppInstance.gotoLoginScreen(transition: true)
                                               // return
                                            }
                                            else if let _ = _error.value(forKey: "code")as? String
                                            {
                                                message =  _error.value(forKey: "message") as? String ?? ""
                                            }
                                            else{
                                                
                                                    //MARK: - this message was handle in faild block of api call
                                                    //showMessage(message: _error.value(forKey: "message")as? String ?? "")
                                                
                                                
                                            }
                                        }
                                    
                                }
                                handler(AlamofireResponse(object: responseData, code: code, message: message))
                            }
                            // handler(AlamofireResponse(object: responseData, code: code, message: message))
                        }
                    }
                    else
                    {
                        if response.response?.statusCode == 401//Unauthorize
                        {
                            //redirect to login screen
                            AppInstance.gotoLoginScreen(transition: true)
                            return
                        }
                        
                        if retryCount == 0
                        {
                            
                            //                            errorHandler(NSError(domain: MSG_SOMETHING_WRONG, code: 0, userInfo: nil))
                            //MARK:- 13/08/21
                            //-- As discuss with bhavin sir and purvaraj sir no need to call Errorlog api --//
                            /*if kCurrentUser.APILog == true
                            {
                                supportURLCall(apiURL: URLFinal, parameters: param, response:response)
                            }*/
//                            showMessage(message: MSG_SOMETHING_WRONG)
                            HideIndicator()
                        }
                        else
                        {
                            usleep(1000)//= 0.01 seconds
                            Alamofiremethod(method, parameter: parameter, Header: Header, retryCount: retryCount-1, isFromLogin: isFromLogin, isSupportURL: isSupportURL, handler: handler, errorHandler: errorHandler)
                        }
                    }
                })
                
            case false:
                print("unreachable, retry no.  -> " + (abs(retryCount-1)).description)
                
                if retryCount == 0
                {
//                    errorHandler(NSError(domain: MSG_NO_INTERNET, code: 0, userInfo: nil))
                    showMessage(message: MSG_NO_INTERNET)
                    HideIndicator()
                }
                else
                {
                    usleep(1000)//= 0.01 seconds
                    Alamofiremethod(method, parameter: parameter, Header: Header, retryCount: retryCount-1, isFromLogin: isFromLogin,isSupportURL: isSupportURL, handler: handler, errorHandler: errorHandler)
                }
            }
        }
    }
    
    
    class func UPLODImage(params: [String: Any],images:[String:UIImage])
    {
        
        var URLFinal = ""
        do
        {
            try URLFinal = baseURL + APIAction.Login.rawValue.asURL().absoluteString
        }
        catch{}
        
        
        let temp :UIImage = images["imagename"]!
        let imgData = temp.jpegData(compressionQuality: CGFloat(1))
        
        
        //            UIImageJPEGRepresentation(image!, 0.7)!
        
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            //Parameter for Upload files
            
            for (key, value) in params
            {
                multipartFormData.append((value as! String).data(using: String.Encoding.utf8)! , withName: key)
            }
            
            for (key,value) in images
            {
                if let data = value.jpegData(compressionQuality: CGFloat(1))
                {
                    //                    multipartFormData.append(data, withName: key, mimeType: "image/jpg")
                    multipartFormData.append(data, withName: key,fileName: "imagename.png" , mimeType: "image/png")
                }
            }
            
        }, usingThreshold:UInt64.init(),
           to: URLFinal, //URL Here
            method: .post,
            headers: [:], //pass header dictionary here
            encodingCompletion: { (result) in
                
                switch result {
                case .success(let upload, _, _):
                    print("the status code is :")
                    
                    //                    upload.uploadProgress(closure: { (progress) in
                    //                        print("something")
                    //                    })
                    
                    upload.responseJSON { response in
                        print("the resopnse code is : \(response.response?.statusCode)")
                        print("the response is : \(response)")
                    }
                    
                case .failure(let encodingError):
                    print("the error is  : \(encodingError.localizedDescription)")
                    break
                }
        })
    }
    
    
    class func AlamofiremethodWithImage(_ method: Alamofire.HTTPMethod, parameter: [String: Any], images: [String: Any], Header: [String: String], retryCount :Int = 3, isAttachment: Bool = false, fileName: String = "imagename.png", fileType: String = "image/png", handler: @escaping CompletionHandler, errorHandler: @escaping ErrorHandler)
    {
        var header = Header
        var URLFinal = ""
        var param : [String:Any] = [String:Any]()
        header["Accept"] = "application/json"
        
        print(header.description)
        
        let rechabilityManager = Alamofire.NetworkReachabilityManager(host: "apple.com")
        
        rechabilityManager?.startListening()
        
        if let r = rechabilityManager
        {
            switch r.isReachable
            {
                
            case true:
                print("reachable")
                let alamofiremanager : Alamofire.SessionManager?
                //var URLFinal = ""
               
                //MARK: - Commented by nuzhat 23/10/2020
                //CheckExpiryDate()
                alamofiremanager = Alamofire.SessionManager.default
                alamofiremanager?.session.configuration.timeoutIntervalForRequest = 31
                alamofiremanager?.session.configuration.timeoutIntervalForResource = 31
                let challengeHandler: ((URLSession, URLAuthenticationChallenge) -> (URLSession.AuthChallengeDisposition, URLCredential?))? = { result, challenge in
                    return (.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
                }
                alamofiremanager?.delegate.sessionDidReceiveChallenge = challengeHandler
                
                //var _parameter = parameter
                //                _parameter["device_id"] = AppInstance.device_token
                //                _parameter["device_type"] = "I"
                print(parameter)
                param = parameter
                
               
                alamofiremanager?.upload(multipartFormData: { (multipartFormData) in
                    for (key,value) in param
                    {
                        multipartFormData.append((value as! String).data(using: String.Encoding.utf8)! , withName: key)
                    }
                    
                   
                        for (key,value) in images
                        {
                            if let data = value as? Data//value.jpegData(compressionQuality: CGFloat(1))
                            {
                                multipartFormData.append(data, withName: key,fileName: fileName , mimeType: fileType)
                            }
                            
                        }
                    
                    
                  
                }, to: URLFinal, method: method, headers: header, encodingCompletion: { (encodingResult) in
                    
                    switch encodingResult
                    {
                        
                    case .success(let upload, _, _):
                        upload.responseJSON(completionHandler: { (response) in
                            print(response.result.value as Any)
                            print(response.result.error as Any)
                            if response.result.isSuccess
                            {
                                if let val = response.result.value
                                {
                                    var message = ""
                                    var code = response.response?.statusCode ?? 200
                                    var responseData: AnyObject = val as AnyObject
                                    
                                    if let _success = (val as AnyObject).value(forKey: "success") as? Int
                                    {
                                        code = _success
                                        if let _code = (val as AnyObject).value(forKey: "code") as? Int
                                        {
                                            if _code == 401//Unauthorize
                                            {
                                                //redirect to login screen
                                                AppInstance.gotoLoginScreen(transition: true)
                                                return
                                                
                                            }
                                        }
                                        
                                        if let _data = (val as AnyObject).value(forKey: "result") as? AnyObject
                                        {
                                            responseData = _data as AnyObject
                                            print(responseData)
                                        }
                                        
                                        if _success == 1
                                        {
                                            if let msg = responseData.value(forKey: "message") as? String
                                            {
                                                message = msg
                                            }
                                        }
                                        else
                                        {
                                            if let _error =  (val as AnyObject).value(forKey: "error") as? [String]
                                            {
                                                if _error.count > 0
                                                {
                                                    message = _error[0]
                                                }
                                            }else if let _error =  (val as AnyObject).value(forKey: "error") as? NSDictionary{
                                                message = _error.value(forKey: "message") as? String ?? ""
                                                 let errorCode = _error.value(forKey: "code")as? Int
                                                 if errorCode == 1556
                                                {
                                                    //APILogout(isAutoLogin: true)
                                                }
                                            }
                                        }
                                    }
                                    handler(AlamofireResponse(object: responseData, code: code, message: message))
                                }
                            }
                            else
                            {
                                if response.response?.statusCode == 401//Unauthorize
                                {
                                    //redirect to login screen
                                    AppInstance.gotoLoginScreen(transition: true)
                                    return
                                }
                                
                                
                                if retryCount == 0
                                {
//                                    errorHandler(NSError(domain: MSG_NO_INTERNET, code: 0, userInfo: nil))
                                    //MARK:- 13/08/21
                                    //-- As discuss with bhavin sir and purvaraj sir no need to call Errorlog api --//
                                    /*if kCurrentUser.APILog == true
                                    {
                                        supportURLCall(apiURL: URLFinal, parameters: param, response:response)
                                    }*/
                                    showMessage(message: MSG_NO_INTERNET)
                                    HideIndicator()
                                }
                                else
                                {
                                    usleep(1000)//= 0.01 seconds
                                    AlamofiremethodWithImage(method, parameter: parameter, images: images, Header: Header, retryCount: retryCount-1, handler: handler, errorHandler: errorHandler)
                                }                    }
                        })
                    case .failure(let encodingError):
                        print(encodingError)
                        errorHandler(encodingError)
                    }
                })
                
            case false:
                print("unreachable, retry no.  -> " + (abs(retryCount-1)).description)
                
                if retryCount == 0
                {
//                    errorHandler(NSError(domain: MSG_NO_INTERNET, code: 0, userInfo: nil))
                    showMessage(message: MSG_NO_INTERNET)
                    HideIndicator()
                }
                else
                {
                    usleep(1000)//= 0.01 seconds
                    AlamofiremethodWithImage(method, parameter: parameter, images: images, Header: Header, retryCount: retryCount-1, handler: handler, errorHandler: errorHandler)
                }
            }
        }
    }
    
    class func rechabilityCheck(maxRetries: Int = 3, rechability: @escaping reachebility)
    {
        let rechabilityManager = Alamofire.NetworkReachabilityManager(host: "apple.com")
        
        rechabilityManager?.startListening()
        
        if let r = rechabilityManager
        {
            switch r.isReachable
            {
                
            case true:
                rechability(true)
            case false:
                if maxRetries == 0
                {
                    rechability(false)
                }
                else
                {
                    usleep(1000)//= 0.01 seconds
                    rechabilityCheck(maxRetries: maxRetries-1, rechability: rechability)
                }
                rechability(false)
            }
        }
    }
}
