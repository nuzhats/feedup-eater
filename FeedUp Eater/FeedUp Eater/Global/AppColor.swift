//
//  AppColor.swift
//  FeedUp Eater
//
//  Created by Jayesh Godhaniya on 21/09/21.
//

import Foundation
import UIKit

let themeColor = UIColor().hexStringToUIColor(hex: "FF7211")
let ShadowColor = UIColor.init(red: 52.0/255, green: 52.0/255, blue: 52.0/255, alpha: 0.2)
let BackgroundColor = UIColor().hexStringToUIColor(hex: "FBFBFB")
let customRedColor = UIColor().hexStringToUIColor(hex: "FF0000")
let DarkSelectedColor = UIColor().hexStringToUIColor(hex: "343434")
let CostomeLightSelectedColor = UIColor().hexStringToUIColor(hex: "F3F3F3")
let DarkUnSelectedColor = UIColor().hexStringToUIColor(hex: "6E6E6E")
let orderTrackigLightColor = UIColor().hexStringToUIColor(hex: "ACACAC")
let BGColor = UIColor().hexStringToUIColor(hex: "FBFBFB")
