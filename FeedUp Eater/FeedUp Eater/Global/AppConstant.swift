//
//  AppConstant.swift
//  FeedUp Eater
//
//  Created by Jayesh Godhaniya on 21/09/21.
//

import Foundation
import UIKit

var AppName = "FeedUp Eater"
var AppStoreURL = "http://itunes.apple.com/app/id"
var PlayStoreURL = ""
var SupportEmail = ""
var passwordLength = 4


///User default object
var DEFAULT = UserDefaults.standard


let APP_VERSION = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
let GoogleMapKey = "AIzaSyDrcDGBNq7OszkcxINQJG32h3MlO0tTS0k"
let GoogleMapDirectionsBaseUrl = "https://maps.googleapis.com/maps/api/directions/json?origin="

struct ScreenSize
{
    static let SCREEN_SIZE = UIScreen.main.bounds.size
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
    static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

///Storyboared name list
enum StoryBoaredNames : String
{
    case main = "Main"
    case authentication = "Authentication"
    case tabbar = "TabBar"
    case home = "Home"
    case cart = "Cart"
    case profile = "Profile"
    case Orders = "Orders"
    case filter = "Filter"
}

let popUp = AppPopUpSuccess()
struct AppPopUpSuccess
{
    var AccountCreate = (title : "", msg: Account_Create, image : #imageLiteral(resourceName: "Successfull"), btnTitle : BTN_Continue)
    var OrderPlaced = (title : LBL_Congratulations, msg: Order_Successfully, image : #imageLiteral(resourceName: "Order placed"), btnTitle : BTN_Yes)
}
let CustomPopUp = CustomAppPopUpDetails()
struct CustomAppPopUpDetails
{
    var LogOut = (title : "", msg : MSG_Logout,isSingle : false, mainTitle : BTN_Logout, otherTitle : BTN_Cancel)
    
    var DeleteAccount = (title : Alert, msg : MSG_Delete_Account,isSingle : false, mainTitle : BTN_Delete, otherTitle : BTN_Cancel)
    
    var Delete = (title : Alert, msg : MSG_Delete_Confirmation,isSingle : false, mainTitle : BTN_Yes, otherTitle : BTN_No)
}
