//
//  StringConstant.swift
//  FeedUp Eater
//
//  Created by Jayesh Godhaniya on 21/09/21.
//

import Foundation


var MSG_NO_INTERNET = "Network connection error!"
var MSG_SOMETHING_WRONG = "Something went wrong!"//////

var Account_Create = "Your account has been successfully created"
var BTN_Continue = "Continue"
var BTN_DONE = "Done"

var MSG_Logout = "Are you sure you want to Logout?"
var BTN_Logout = "Logout"
var BTN_Cancel = "Cancel"

var Alert = "Alert"
var MSG_Delete_Account = "Are you sure you want to Delete Account?"
var BTN_Delete = "Delete"

var MSG_Delete_Confirmation = "Are you sure you want to Delete?"
var BTN_Yes = "Yes"
var BTN_No = "No"


var INVALID_EMAIL = "Please enter valid email"
var INVALID_PASSWORD = "Please enter valid password"
var PASSWORD_NOT_MATCH = "Password and confirm password does not matches"
var INVALID_PHONE = "Please enter valid phone number"

var ENTER_EMAIL = "the email address cannot be blank"
var ENTER_PASSWORD = "the password cannot be blank"
var ENTER_NAME = "the name cannot be blank"
var ENTER_CONFIRMPASSWORD = "the confirm password cannot be blank"
var ENTER_PHONE = "Please enter phone number"
var Allergies_Detail = "In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content"

var lbl_Items = "Items"

//Go With Subscription
var lbl_Free_Delivery = "- Unlimited Free Delivery"
var lbl_Extra_Discount = "- Extra Discount"
var lbl_Extra_Benefits = "- Extra Benefits"
 
//Delivery Option
var Leave_at_door = "Leave at door"
var Meet_at_door = "Meet at door"
var Meet_outside = "Meet outside"

var Add_Instruction = "Add Instruction"
var Add_Label = "Add a label (ex. office)"

var Delivery_Option = "Delivery Option"
var Btn_Update = "Update"
var Btn_Add_Address = "Add Address"

//Order Tracking
var Order_Accepted = "Order Accepted"
var Food_prepared = "Food is being prepared"
var Food_on_way = "Food is on the way"
var Order_arrived = "Order arrived"

var BTN_Submit = "Submit"
var Add_text = "Add text"

var LBL_Congratulations = "Congratulations"
var Order_Successfully = "Your order has been placed successfully."

var LBL_Packaging = "Packaging"
var LBL_Price = "Price"

var Select_Image = "Select Image"
var Take_Pic = "Take Pic"
var Choose_from_Gallery = "Choose from Gallery"
