//
//  AppDelegate+Extension.swift
//  FeedUp Eater
//
//  Created by Jayesh Godhaniya on 21/09/21.
//

import Foundation
import UserNotifications
import Firebase


var isPushEnabled : Bool = false


//MARK:- AppDelegate Remote Notification Manager
extension AppDelegate: UNUserNotificationCenterDelegate
{
    func configureForRemoteNotification()
    {
        // iOS 10 support
        if #available(iOS 10.0, *)
        {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .sound]
            UIApplication.shared.registerForRemoteNotifications()//registerUserNotificationSettings(authOptions)
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {success, _ in
                    
            })
            
            // For iOS 10 data message (sent via FCM)
            Messaging.messaging().delegate = self
            
        }
        else
        {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        UIApplication.shared.registerForRemoteNotifications()
        
    }
    
    //MARK:- Notification Delegate -
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        // check environment
        let _type : MessagingAPNSTokenType = isDevelopmentProvisioningProfile() ? .sandbox : .prod
        
        // set token to firebase instance
        Messaging.messaging().setAPNSToken(deviceToken, type: _type)
        if Messaging.messaging().fcmToken != nil
        {
            self.device_token = Messaging.messaging().fcmToken!
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error)
    {
        AppInstance.device_token = UUID().uuidString
        
    }
    
    func manageAPIToken(isPushEnable : Bool, completion : ((Bool) -> Void)?)
    {
        isPushEnabled = isPushEnable
        if isPushEnable
        {
            configureForRemoteNotification()
            completion?(true)
        }
        
    }
    
    
    func isDevelopmentProvisioningProfile() -> Bool
    {
        #if IOS_SIMULATOR
        return true
        #else
        // there will be no provisioning profile in AppStore Apps
        guard let fileName = Bundle.main.path(forResource: "embedded", ofType: "mobileprovision") else {
            return false
        }
        
        let fileURL = URL(fileURLWithPath: fileName)
        // the documentation says this file is in UTF-8, but that failed
        // on my machine. ASCII encoding worked ¯\_(ツ)_/¯
        guard let data = try? String(contentsOf: fileURL, encoding: .ascii) else {
            return false
        }
        
        let cleared = data.components(separatedBy: .whitespacesAndNewlines).joined()
        
        return cleared.contains("<key>get-task-allow</key><true/>")
        #endif
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        let userInfo = notification.request.content.userInfo
        print(userInfo)
        
        NotificationCenter.default.post(name: Notification.Name("UpdateNotificationList"), object: nil)
        completionHandler([.alert, .sound])
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void)
    {
        let userInfo = response.notification.request.content.userInfo
        print(userInfo)
        
        completionHandler()
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        print(userInfo)
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
    }
    func application(application: UIApplication,  didReceiveRemoteNotification userInfo: [NSObject : AnyObject],  fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        
    }
    
    
}
extension AppDelegate : MessagingDelegate
{
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?)
    {
        device_token = fcmToken ?? ""
        
    }
}
