//
//  FavouriteCVC.swift
//  FeedUp Eater
//
//  Created by Jayesh Godhaniya on 29/09/21.
//

import UIKit

class FavouriteCVC: UICollectionViewCell {
    @IBOutlet var imgResturentDish: UIImageView!
    @IBOutlet var lblRating: UILabel!
    @IBOutlet var imgLikeDisLike: UIImageView!
    @IBOutlet var imgCertified: UIImageView!
    @IBOutlet var constImgCertifiedWidth: NSLayoutConstraint!
    @IBOutlet var lblResturentDishName: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblDetails: UILabel!
    
}
