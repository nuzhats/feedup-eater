//
//  SettingVC.swift
//  FeedUp Eater
//
//  Created by Variance on 23/09/21.
//

import UIKit

class SettingVC: UIViewController {
    
    //MARK:- Outlets --

    
    //MARK:- View Life Cycle --
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    
    // MARK: - Button Action Method --
    @IBAction func btnAccountSetting(_ sender: Any)
    {
        let VC: AccountSettingVC = Utilities.viewController(name: "AccountSettingVC", onStoryBoared: StoryBoaredNames.profile.rawValue) as! AccountSettingVC
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func btnPermission(_ sender: Any)
    {
        /*let VC: SortAndFilterVC = Utilities.viewController(name: "SortAndFilterVC", onStoryBoared: StoryBoaredNames.filter.rawValue) as! SortAndFilterVC
        self.navigationController?.pushViewController(VC, animated: true)*/
    }
    
    

}
