//
//  AccountSettingVC.swift
//  FeedUp Eater
//
//  Created by Variance on 23/09/21.
//

import UIKit
import GrowingTextView
import Photos

class AccountSettingVC: UIViewController {
    
    //MARK:- Outlets --
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var btnEditProfileImage: UIButton!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPhoneNo: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var tvAddress: GrowingTextView!
    @IBOutlet weak var btnAddAddress: UIButton!
    @IBOutlet weak var btnDeleteAccount: UIButton!
    
    //MARK:- Varibles --
    let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
    
    //MARK:- View Life Cycle --
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        SetUpView()
    }
    //MARK:- SetUp View
    func SetUpView()
    {
        btnAddAddress.underlineButtonText(textColor: themeColor)
        btnDeleteAccount.underlineButtonText(textColor: customRedColor)
    }
    
    //MARK:- Select Image --
    func SelectImage()
    {
        let alert = UIAlertController(title: Select_Image, message: nil, preferredStyle: .actionSheet)
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        alert.addAction(UIAlertAction(title: Take_Pic, style: .default, handler: { _ in
            switch self.cameraAuthorizationStatus
            {
            case .denied:
                print("Camera permission")
            case .authorized:
                self.openCamera()
            case .restricted:
                print("Camera permission")
            case .notDetermined:
                // Prompting user for the permission to use the camera.
                AVCaptureDevice.requestAccess(for: AVMediaType.video) { granted in
                    if granted
                    {
                        //print("Granted access to \(cameraMediaType)")
                        self.openCamera()
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            print("Camera permission")
                        }
                        // print("Denied access to \(cameraMediaType)")
                        
                    }
                }
            @unknown default:
                print("Error")
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: Choose_from_Gallery, style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: BTN_Cancel, style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    //MARK:- Open camera --
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)
        {
            DispatchQueue.main.async {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerController.SourceType.camera
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        else
        {
            print("Camera permission")
        }
    }
    
    //MARK:- Open gallary
    func openGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary)
        {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            print("Camera permission")
        }
    }

    // MARK: - Button Action Method --
    @IBAction func btnEditProfileImage(_ sender: Any)
    {
        SelectImage()
    }
    
    @IBAction func btnAddAddress(_ sender: Any)
    {
        let VC: AddAddressVC = Utilities.viewController(name: "AddAddressVC", onStoryBoared: StoryBoaredNames.cart.rawValue) as! AddAddressVC
        VC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func btnDeleteAccount(_ sender: Any)
    {
        DeleteAccountPopUp()
    }
    
    @IBAction func btnSave(_ sender: Any)
    {
    }
    
    //MARK:- popup --
    func DeleteAccountPopUp()
    {
        let vc = Utilities.viewController(name: "CustomePopUpVC", onStoryBoared: StoryBoaredNames.authentication.rawValue) as! CustomePopUpVC
        vc.popup = CustomPopUp.DeleteAccount
        
        
        vc.completionHandler = {
            (type: Int) -> () in
            
            if type == 1
            {
                // perform cancel action
            }
            else
            {
                AppInstance.gotoLoginScreen(transition: false)
            }
            vc.dismiss(animated: true, completion: nil)
            
        }
        self.present(vc, animated: true, completion: nil)
    }
    

}
//MARK:- UITextField Delegate --
extension AccountSettingVC : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == txtName
        {
            txtPhoneNo.becomeFirstResponder()
        }
        else if textField == txtPhoneNo
        {
            txtEmail.becomeFirstResponder()
        }
        else if textField == txtEmail
        {
            tvAddress.becomeFirstResponder()
        }
        else
        {
            self.view.endEditing(true)
        }
        return true
    }
}
//MARK: - Image Picekr Delegate Methods -
extension AccountSettingVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        var pickedImage = UIImage()
        if let img1 = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
        {
            pickedImage = img1
            
        }
        else if let img1 = info[.originalImage] as? UIImage
        {
            pickedImage = img1
        }
        
        imgUser.image = pickedImage
                
        picker.dismiss(animated: true, completion: nil)
    }
    
}
