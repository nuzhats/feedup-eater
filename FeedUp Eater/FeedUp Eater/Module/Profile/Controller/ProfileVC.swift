//
//  ProfileVC.swift
//  FeedUp Eater
//
//  Created by Variance on 22/09/21.
//

import UIKit

class ProfileVC: UIViewController {

    //MARK:- Outlets --
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblMobileNo: UILabel!
    
    //MARK:- View Life Cycle --
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }

    // MARK: - Button Action Method --
    @IBAction func btnSetting(_ sender: Any)
    {
        let VC: SettingVC = Utilities.viewController(name: "SettingVC", onStoryBoared: StoryBoaredNames.profile.rawValue) as! SettingVC
        VC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func btnNotification(_ sender: Any)
    {
        let VC: NotificationListVC = Utilities.viewController(name: "NotificationListVC", onStoryBoared: StoryBoaredNames.profile.rawValue) as! NotificationListVC
        VC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func btnPayment(_ sender: Any)
    {
        
    }
    
    @IBAction func btnFavourite(_ sender: Any)
    {
        /*let VC: FilterSearchVC = Utilities.viewController(name: "FilterSearchVC", onStoryBoared: StoryBoaredNames.filter.rawValue) as! FilterSearchVC
        VC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(VC, animated: true)*/
        let VC: FavouriteVC = Utilities.viewController(name: "FavouriteVC", onStoryBoared: StoryBoaredNames.profile.rawValue) as! FavouriteVC
        VC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(VC, animated: true)
    }

    @IBAction func btnManageAddress(_ sender: Any)
    {
        let VC: ManageAddressVC = Utilities.viewController(name: "ManageAddressVC", onStoryBoared: StoryBoaredNames.profile.rawValue) as! ManageAddressVC
        VC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func btnReviews(_ sender: Any)
    {
        let VC: ReviewListVC = Utilities.viewController(name: "ReviewListVC", onStoryBoared: StoryBoaredNames.profile.rawValue) as! ReviewListVC
        VC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func btnLogOut(_ sender: Any)
    {
        LogOutPopUp()
    }
    @IBAction func btnReferralAndShare(_ sender: Any)
    {
        let textToShare = "Check out my app"

        if let myWebsite = URL(string: AppStoreURL) {//Enter link to your app here
            let objectsToShare = [textToShare, myWebsite, #imageLiteral(resourceName: "Logo")] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)

            //Excluded Activities
            activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
            //

            activityVC.popoverPresentationController?.sourceView = sender as? UIView
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    @IBAction func btnRunningOrder(_ sender: Any)
    {
        let VC: OrderTrackingVC = Utilities.viewController(name: "OrderTrackingVC", onStoryBoared: StoryBoaredNames.cart.rawValue) as! OrderTrackingVC
        VC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    //MARK:- popup --
    func LogOutPopUp()
    {
        let vc = Utilities.viewController(name: "CustomePopUpVC", onStoryBoared: StoryBoaredNames.authentication.rawValue) as! CustomePopUpVC
        vc.popup = CustomPopUp.LogOut
        
        self.tabBarController?.tabBar.isHidden = true
        vc.completionHandler = {
            (type: Int) -> () in
            
            if type == 1
            {
                // perform cancel action
                self.tabBarController?.tabBar.isHidden = false
            }
            else
            {
                AppInstance.gotoLoginScreen(transition: false)
            }
            vc.dismiss(animated: true, completion: nil)
            
        }
        self.present(vc, animated: true, completion: nil)
    }

}
