//
//  ManageAddressVC.swift
//  FeedUp Eater
//
//  Created by Variance on 23/09/21.
//

import UIKit

class ManageAddressVC: UIViewController {
    
    //MARK:- Outlets --
    @IBOutlet weak var tblSavedAddressList: UITableView!
    
    //MARK:- View Life Cycle --
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    
    // MARK: - Button Action Method --
    @IBAction func btnAddAddress(_ sender: Any)
    {
        let VC: AddAddressVC = Utilities.viewController(name: "AddAddressVC", onStoryBoared: StoryBoaredNames.cart.rawValue) as! AddAddressVC
        VC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    //MARK:- popup --
    func DeleteAddressPopUp()
    {
        let vc = Utilities.viewController(name: "CustomePopUpVC", onStoryBoared: StoryBoaredNames.authentication.rawValue) as! CustomePopUpVC
        vc.popup = CustomPopUp.Delete
        
        
        vc.completionHandler = {
            (type: Int) -> () in
            
            if type == 1
            {
                // perform cancel action
            }
            else
            {
                
            }
            vc.dismiss(animated: true, completion: nil)
            
        }
        self.present(vc, animated: true, completion: nil)
    }

}
//MARK:- Tableview Delegate & Datasource Method --
extension ManageAddressVC : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SavedAddressListTVC", for: indexPath) as! SavedAddressListTVC
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let DELETE = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                // Call delete action
            self.DeleteAddressPopUp()
            success(true)
        })
            // here set your image and background color
        DELETE.image = #imageLiteral(resourceName: "Delete")
        
        DELETE.backgroundColor = BackgroundColor
            
        let EDIT = UIContextualAction(style: .normal, title: "") { action, view, complete in
                
            let VC: AddAddressVC = Utilities.viewController(name: "AddAddressVC", onStoryBoared: StoryBoaredNames.cart.rawValue) as! AddAddressVC
            VC.isEditAddress = true
            VC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(VC, animated: true)
            complete(true)
        }
        EDIT.image = #imageLiteral(resourceName: "Edit")
        EDIT.backgroundColor = BackgroundColor
            
            
        return UISwipeActionsConfiguration(actions: [DELETE, EDIT])
        
    }
    
    
}
