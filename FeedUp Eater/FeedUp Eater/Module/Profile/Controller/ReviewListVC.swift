//
//  ReviewListVC.swift
//  FeedUp Eater
//
//  Created by Variance on 23/09/21.
//

import UIKit

class ReviewListVC: UIViewController {

    //MARK:- Outlets --
    @IBOutlet weak var tblReviewList: UITableView!
    
    
    //MARK:- View Life Cycle --
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK:- Cell Button Action Method --
    @objc func btnDelete(sender: UIButton)
    {
        DeleteReviewPopUp()
    }

    //MARK:- popup --
    func DeleteReviewPopUp()
    {
        let vc = Utilities.viewController(name: "CustomePopUpVC", onStoryBoared: StoryBoaredNames.authentication.rawValue) as! CustomePopUpVC
        vc.popup = CustomPopUp.Delete
        
        
        vc.completionHandler = {
            (type: Int) -> () in
            
            if type == 1
            {
                // perform cancel action
            }
            else
            {
                
            }
            vc.dismiss(animated: true, completion: nil)
            
        }
        self.present(vc, animated: true, completion: nil)
    }

}
//MARK:- Tableview Delegate & Datasource Method --
extension ReviewListVC : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewListTVC", for: indexPath) as! ReviewListTVC
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(btnDelete), for: .touchUpInside)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    
}
