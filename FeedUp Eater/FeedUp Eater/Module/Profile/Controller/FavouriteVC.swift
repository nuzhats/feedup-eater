//
//  FavouriteVC.swift
//  FeedUp Eater
//
//  Created by Jayesh Godhaniya on 29/09/21.
//

import UIKit

class FavouriteVC: UIViewController {

    //MARK:- Outlets --
    @IBOutlet var cvFavouriteList: UICollectionView!
    
    //MARK:- View Life Cycle --
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK:- Button Action Methods --
    

}
//MARK:- Collectionviee Datasource & Delegate
extension FavouriteVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 20
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
       
        let cell: FavouriteCVC = collectionView.dequeueReusableCell(withReuseIdentifier: "FavouriteCVC", for: indexPath) as! FavouriteCVC
        
        
        return cell
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let VC: RestaurantDetailVC = Utilities.viewController(name: "RestaurantDetailVC", onStoryBoared: StoryBoaredNames.filter.rawValue) as! RestaurantDetailVC
        VC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let HeightWidth = (cvFavouriteList.frame.size.width - 20) / 2
        return CGSize(width: HeightWidth, height: HeightWidth + 35)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }

    
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
   
}
