//
//  RestaurantDetailModel.swift
//  FeedUp Eater
//
//  Created by Variance on 25/09/21.
//

import Foundation
import SwiftyJSON

class RestaurantDetailModel : BaseModel {
    
    var indexInt: Int = 0
    var Count: Int = 0
    
    
    enum CodingKeys: String, CodingKey {
        case indexInt = "indexInt"
        case Count = "Count"
        
    }
    
    override init() {
        super.init()
    }
    
    override init(info: JSON) {
        super.init(info: info)
        
        indexInt = info[CodingKeys.indexInt.rawValue].int ?? 0
        Count = info[CodingKeys.Count.rawValue].int ?? 0
        
        
    }
    
    
}
