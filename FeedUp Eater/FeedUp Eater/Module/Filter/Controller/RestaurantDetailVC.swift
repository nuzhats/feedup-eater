//
//  RestaurantDetailVC.swift
//  FeedUp Eater
//
//  Created by Variance on 25/09/21.
//

import UIKit

class RestaurantDetailVC: UIViewController {

    @IBOutlet weak var lblResturent: UILabel!
    @IBOutlet weak var imgResturent: UIImageView!
    @IBOutlet weak var tblFoodDishList: UITableView!
    @IBOutlet weak var lblResturentDetails: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var viewRestaurantDetails: UIView!
    @IBOutlet weak var cvCategory: UICollectionView!
    @IBOutlet weak var btnAllergiesDetails: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnVeg: UIButton!
    @IBOutlet weak var btnNonVeg: UIButton!
    @IBOutlet weak var lblAllergiesDetails: UILabel!
    @IBOutlet weak var constAllergiesBottom: NSLayoutConstraint!
    //@IBOutlet weak var viewAddToCart: UIView!
    @IBOutlet weak var viewAddToCart: GradientView!
    @IBOutlet weak var constViewAddToCartHeight: NSLayoutConstraint!
    @IBOutlet weak var lblNoOfItems: UILabel!
    
    //MARK:- Outlets --
    var CetagoryList = ["Today's Meal","Indian","Breakfast","Frozen"]
    var AddData : [RestaurantDetailModel] = [RestaurantDetailModel]()
    var SelectedInddex = [Int]()
    
    //MARK:- View Life Cycle --
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ManageBottomCartView()
        viewAddToCart.isHidden = true
        constViewAddToCartHeight.constant = 0
    }
    
    //MARK:- Button Action Methods --
    @IBAction func btnAllergiesDetails(_ sender: Any)
    {
        if btnAllergiesDetails.isSelected
        {
            lblAllergiesDetails.text = ""
            btnAllergiesDetails.isSelected = false
            constAllergiesBottom.constant = 5
            btnAllergiesDetails.setImage(#imageLiteral(resourceName: "Dropdown"), for: .normal)
        }
        else
        {
            lblAllergiesDetails.text = Allergies_Detail
            btnAllergiesDetails.isSelected = true
            constAllergiesBottom.constant = 10
            btnAllergiesDetails.setImage(#imageLiteral(resourceName: "Collapse"), for: .normal)
        }
    }
    
    @IBAction func btnSearch(_ sender: Any)
    {
    }
    
    @IBAction func btnFilter(_ sender: Any)
    {
        let VC: SortAndFilterVC = Utilities.viewController(name: "SortAndFilterVC", onStoryBoared: StoryBoaredNames.filter.rawValue) as! SortAndFilterVC
        VC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func btnVeg(_ sender: Any)
    {
        btnNonVeg.setImage(#imageLiteral(resourceName: "Nonveg 01"), for: .normal)
        btnVeg.setImage(#imageLiteral(resourceName: "Veg selected"), for: .normal)
    }
    
    @IBAction func btnNonVeg(_ sender: Any)
    {
        btnVeg.setImage(#imageLiteral(resourceName: "veg 01"), for: .normal)
        btnNonVeg.setImage(#imageLiteral(resourceName: "Nonveg selected"), for: .normal)
    }
    @IBAction func btnAddToCart(_ sender: Any) {
    }
    
    //MARK:- Cell Button Action Method --
    @objc func btnAdd(sender: UIButton)
    {
        let data = RestaurantDetailModel()
        data.indexInt = sender.tag
        data.Count = 1
        AddData.append(data)
        ReloadIndex(ind: sender.tag)
    }
    @objc func btnMinus(sender: UIButton)
    {
        if let index = AddData.firstIndex(where: {$0.indexInt == sender.tag})
        {
            AddData[index].Count -= 1
            if AddData[index].Count == 0
            {
                AddData.remove(at: index)
            }
        }
        ReloadIndex(ind: sender.tag)
    }
    @objc func btnPlus(sender: UIButton)
    {
        if let index = AddData.firstIndex(where: {$0.indexInt == sender.tag})
        {
            AddData[index].Count += 1
        }
        ReloadIndex(ind: sender.tag)
    }
    //MARK:- Reload Index Path --
    func ReloadIndex(ind: Int)
    {
        let indexPath = IndexPath(row: ind, section: 0)
        tblFoodDishList.reloadRows(at: [indexPath], with: .automatic)
        //Manage Add to cart view --
        ManageBottomCartView()
    }
    func ManageBottomCartView()
    {
        var TotalItems = 0
        for Obj in AddData
        {
            TotalItems += Obj.Count
        }
        if TotalItems == 0
        {
            viewAddToCart.isHidden = true
            constViewAddToCartHeight.constant = 0
        }
        else
        {
            viewAddToCart.isHidden = false
            constViewAddToCartHeight.constant = 95
            lblNoOfItems.text = "\(TotalItems) " + lbl_Items
        }
    }


}
//MARK:- Tableview Delegate & Datasource Method --
extension RestaurantDetailVC : UITableViewDelegate, UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailFoodListTVC", for: indexPath) as! DetailFoodListTVC
            
        cell.btnAdd.tag = indexPath.row
        cell.btnAdd.addTarget(self, action: #selector(btnAdd), for: .touchUpInside)
        
        cell.btnMinus.tag = indexPath.row
        cell.btnMinus.addTarget(self, action: #selector(btnMinus), for: .touchUpInside)
        
        cell.btnPlus.tag = indexPath.row
        cell.btnPlus.addTarget(self, action: #selector(btnPlus), for: .touchUpInside)
        
        if let ind = AddData.firstIndex(where: {$0.indexInt == indexPath.row})
        {
            cell.lblQuantity.text = "\(AddData[ind].Count)"
            cell.viewAddMore.isHidden = false
            cell.viewAdd.isHidden = true
        }
        else
        {
            cell.viewAddMore.isHidden = true
            cell.viewAdd.isHidden = false
        }
        
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let VC: FoodExtraIngredientsVC = Utilities.viewController(name: "FoodExtraIngredientsVC", onStoryBoared: StoryBoaredNames.filter.rawValue) as! FoodExtraIngredientsVC
        VC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    
}
//MARK:- Collectionviee Datasource & Delegate
extension RestaurantDetailVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return CetagoryList.count
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
       
        let cell: FoodYouLoveCVC = collectionView.dequeueReusableCell(withReuseIdentifier: "FoodYouLoveCVC", for: indexPath) as! FoodYouLoveCVC
        let Obj = CetagoryList[indexPath.row]
        cell.lblName.text = Obj
        if SelectedInddex.contains(indexPath.row)
       {
           cell.viewHolder.backgroundColor = themeColor
           cell.lblName.textColor = .white
       }
       else
       {
           cell.viewHolder.backgroundColor = CostomeLightSelectedColor
           cell.lblName.textColor = DarkUnSelectedColor
       }
        return cell
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if let index = SelectedInddex.firstIndex(where: {$0 == indexPath.row})
       {
           SelectedInddex.remove(at: index)
       }
       else
       {
           SelectedInddex.append(indexPath.row)
       }
        //collectionView.reloadData()
        let indexPath = IndexPath(item: indexPath.row, section: 0)
        collectionView.reloadItems(at: [indexPath])
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        //Set cell daynamic width accroding module name
        let Obj = CetagoryList[indexPath.row]
        let label = UILabel(frame: CGRect.zero)
        label.font = UIFont(name: "SourceSansPro-Semibold", size: 16.0)
        label.text = Obj
        label.sizeToFit()
        return CGSize(width: label.frame.width + 30, height: 40)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }

    
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
   
}
