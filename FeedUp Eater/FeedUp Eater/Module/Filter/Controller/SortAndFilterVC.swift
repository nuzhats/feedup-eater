//
//  SortAndFilterVC.swift
//  FeedUp Eater
//
//  Created by Variance on 23/09/21.
//

import UIKit

class SortAndFilterVC: UIViewController {

    //MARK:- Outlets --
    @IBOutlet weak var lblPickedforyou: UILabel!
    @IBOutlet weak var btnPickedforyou: UIButton!
    @IBOutlet weak var imgPickedforyou: UIImageView!
    @IBOutlet weak var lblMostPopular: UILabel!
    @IBOutlet weak var btnMostPopular: UIButton!
    @IBOutlet weak var imgMostPopular: UIImageView!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var btnRating: UIButton!
    @IBOutlet weak var imgRating: UIImageView!
    @IBOutlet weak var lblDeliveryTime: UILabel!
    @IBOutlet weak var btnDeliveryTime: UIButton!
    @IBOutlet weak var imgDeliveryTime: UIImageView!
    @IBOutlet weak var switchTopFeeders: UISwitch!
    @IBOutlet weak var lblVegetarin: UILabel!
    @IBOutlet weak var btnVegetarin: UIButton!
    @IBOutlet weak var imgVegetarin: UIImageView!
    @IBOutlet weak var lblVegan: UILabel!
    @IBOutlet weak var btnVegan: UIButton!
    @IBOutlet weak var imgVegen: UIImageView!
    @IBOutlet weak var lblGlutenfree: UILabel!
    @IBOutlet weak var btnGlutenfree: UIButton!
    @IBOutlet weak var imgGlutenfree: UIImageView!
    @IBOutlet weak var lblHalal: UILabel!
    @IBOutlet weak var btnHalal: UIButton!
    @IBOutlet weak var imgHalal: UIImageView!
    @IBOutlet weak var lblAllergyFriendly: UILabel!
    @IBOutlet weak var btnAllergyFriendly: UIButton!
    @IBOutlet weak var imgAllergyFriendly: UIImageView!
    
    
    @IBOutlet weak var sliderPrice: CustomSlider!
    @IBOutlet weak var lblPriceRange: UILabel!
    @IBOutlet weak var sliderDeliveryFee: CustomSlider!
    @IBOutlet weak var lblDeliveryFee: UILabel!
    @IBOutlet weak var lblDeliveryFeePrice: UILabel!
    
    //MARK:- View Life Cycle --
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        btnPickedforyou.isSelected = true
        updatePriceValue()
        updateDeliveryFeeValue()
    }
    

    //MARK:- Button Action Method --
    @IBAction func btnPickedforyou(_ sender: Any)
    {
        if btnPickedforyou.isSelected
        {
            lblPickedforyou.textColor = DarkSelectedColor
            btnPickedforyou.isSelected = false
        }
        else
        {
            lblPickedforyou.textColor = themeColor
            btnPickedforyou.isSelected = true
        }
    }
    
    @IBAction func btnMostPopular(_ sender: Any)
    {
        if btnMostPopular.isSelected
        {
            lblMostPopular.textColor = DarkSelectedColor
            btnMostPopular.isSelected = false
        }
        else
        {
            lblMostPopular.textColor = themeColor
            btnMostPopular.isSelected = true
        }
    }
    
    @IBAction func btnRating(_ sender: Any)
    {
        if btnRating.isSelected
        {
            lblRating.textColor = DarkSelectedColor
            btnRating.isSelected = false
        }
        else
        {
            lblRating.textColor = themeColor
            btnRating.isSelected = true
        }
    }
    
    @IBAction func btnDeliveryTime(_ sender: Any)
    {
        if btnDeliveryTime.isSelected
        {
            lblDeliveryTime.textColor = DarkSelectedColor
            btnDeliveryTime.isSelected = false
        }
        else
        {
            lblDeliveryTime.textColor = themeColor
            btnDeliveryTime.isSelected = true
        }
    }
    
    @IBAction func btnSubscriptionPlans(_ sender: Any)
    {
        let VC: FilterSearchVC = Utilities.viewController(name: "FilterSearchVC", onStoryBoared: StoryBoaredNames.filter.rawValue) as! FilterSearchVC
        VC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func btnVegetarin(_ sender: Any)
    {
        if btnVegetarin.isSelected
        {
            lblVegetarin.textColor = DarkSelectedColor
            btnVegetarin.isSelected = false
        }
        else
        {
            lblVegetarin.textColor = themeColor
            btnVegetarin.isSelected = true
        }
    }
    
    @IBAction func btnVegan(_ sender: Any)
    {
        if btnVegan.isSelected
        {
            lblVegan.textColor = DarkSelectedColor
            btnVegan.isSelected = false
        }
        else
        {
            lblVegan.textColor = themeColor
            btnVegan.isSelected = true
        }
    }
    
    @IBAction func btnGlutenfree(_ sender: Any)
    {
        if btnGlutenfree.isSelected
        {
            lblGlutenfree.textColor = DarkSelectedColor
            btnGlutenfree.isSelected = false
        }
        else
        {
            lblGlutenfree.textColor = themeColor
            btnGlutenfree.isSelected = true
        }
    }
    
    @IBAction func btnHalal(_ sender: Any)
    {
        if btnHalal.isSelected
        {
            lblHalal.textColor = DarkSelectedColor
            btnHalal.isSelected = false
        }
        else
        {
            lblHalal.textColor = themeColor
            btnHalal.isSelected = true
        }
    }
    
    @IBAction func btnAllergyFriendly(_ sender: Any)
    {
        if btnAllergyFriendly.isSelected
        {
            lblAllergyFriendly.textColor = DarkSelectedColor
            btnAllergyFriendly.isSelected = false
        }
        else
        {
            lblAllergyFriendly.textColor = themeColor
            btnAllergyFriendly.isSelected = true
        }
    }
    
    //MARK:- Switch Changes --
    @IBAction func switchTopFeeders(_ sender: Any)
    {
    }
    
    // MARK:- Slider Valuew Changed Method --
    @IBAction func sliderPrice(_ sender: Any)
    {
        updatePriceValue()
    }
    
    @IBAction func sliderDeliveryFee(_ sender: Any)
    {
        updateDeliveryFeeValue()
    }
    
    //MARK:- Update Slider Value --
    //Manage label frame accroding to slider value
    func updatePriceValue()
    {
       let IntValue:Int = Int(sliderPrice.value)//Double = Double(sliderPrice.value * 10).rounded() / 10
       lblPriceRange.text = "$\(IntValue)"
       let trackRect: CGRect  = sliderPrice.trackRect(forBounds: sliderPrice.bounds)
       let thumbRect: CGRect  = sliderPrice.thumbRect(forBounds: sliderPrice.bounds , trackRect: trackRect, value: sliderPrice.value)
       let x = thumbRect.origin.x + sliderPrice.frame.origin.x + 23
       let y = sliderPrice.frame.origin.y - 15
       lblPriceRange.center = CGPoint(x: x, y: y)
    }
    func updateDeliveryFeeValue()
    {
        let IntValue:Int = Int(sliderDeliveryFee.value)//Double = Double(sliderDeliveryFee.value * 10).rounded() / 10
        lblDeliveryFee.text = "$\(IntValue)"
        lblDeliveryFeePrice.text = "$\(IntValue)"
        let trackRect: CGRect  = sliderDeliveryFee.trackRect(forBounds: sliderDeliveryFee.bounds)
        let thumbRect: CGRect  = sliderDeliveryFee.thumbRect(forBounds: sliderDeliveryFee.bounds , trackRect: trackRect, value: sliderDeliveryFee.value)
        let x = thumbRect.origin.x + sliderDeliveryFee.frame.origin.x + 23
        let y = sliderDeliveryFee.frame.origin.y - 15
        lblDeliveryFee.center = CGPoint(x: x, y: y)
    }
}
