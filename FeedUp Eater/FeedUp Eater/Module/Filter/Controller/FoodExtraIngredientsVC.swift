//
//  FoodExtraIngredientsVC.swift
//  FeedUp Eater
//
//  Created by Variance on 25/09/21.
//

import UIKit

class FoodExtraIngredientsVC: UIViewController {
    
    //MARK:- View Life Cycle --
    @IBOutlet weak var imgFood: UIImageView!
    @IBOutlet weak var lblFoodName: UILabel!
    @IBOutlet weak var imgVegNonVeg: UIImageView!
    @IBOutlet weak var lblDetails: UILabel!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var txtNote: TextViewWithPlaceholder!
    @IBOutlet weak var tblSubscription: UITableView!
    @IBOutlet weak var tblExtraIngredients: UITableView!
    @IBOutlet weak var constTblSubscriptionHeight: NSLayoutConstraint!
    @IBOutlet weak var constTblExtraIngredientsHeight: NSLayoutConstraint!
    @IBOutlet weak var lblSubscribePrice: UILabel!
    

    //MARK:- Varible --
    var TotalQuantity = 1
    let TblExtraIngredientsCount = 3
    let SubscriptionTypeArray = [lbl_Free_Delivery,lbl_Extra_Discount,lbl_Extra_Benefits]
    var SelectedExtraIngredients = [0]
    
    //MARK:- View Life Cycle --
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lblQuantity.text = "\(TotalQuantity)"
        ManageTabaleHeight()
    }
    
    //MARK:- Manage Table Height
    func ManageTabaleHeight()
    {
        constTblExtraIngredientsHeight.constant = CGFloat(TblExtraIngredientsCount * 45)//Change TblExtraIngredientsCount to array count
        constTblSubscriptionHeight.constant = CGFloat(SubscriptionTypeArray.count * 35)
    }
    

    //MARK:- Button Action Methods --
    @IBAction func btnMinus(_ sender: Any)
    {
        TotalQuantity -= 1
        if TotalQuantity == 0
        {
            btnMinus.isUserInteractionEnabled = false
        }
        lblQuantity.text = "\(TotalQuantity)"
    }
    
    @IBAction func btnPlus(_ sender: Any)
    {
        TotalQuantity += 1
        if TotalQuantity > 0
        {
            btnMinus.isUserInteractionEnabled = true
        }
        lblQuantity.text = "\(TotalQuantity)"
    }
    
    @IBAction func btnSubscribe(_ sender: Any)
    {
    }
    
    @IBAction func btnAddToCart(_ sender: Any)
    {
    }
    
    //MARK:- Cell Button Action Method --
    @objc func btnCheckBox(sender: UIButton)
    {
        if let index = SelectedExtraIngredients.firstIndex(where: {$0 == sender.tag})
        {
            SelectedExtraIngredients.remove(at: index)
        }
        else
        {
            SelectedExtraIngredients.append(sender.tag)
        }
        let indexPath = IndexPath(row: sender.tag, section: 0)
        tblExtraIngredients.reloadRows(at: [indexPath], with: .automatic)
    }
   

}
//MARK:- Tableview Delegate & Datasource Method --
extension FoodExtraIngredientsVC : UITableViewDelegate, UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == tblExtraIngredients
        {
            return TblExtraIngredientsCount
        }
        return SubscriptionTypeArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if tableView == tblExtraIngredients
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FoodExtraIngredientsTVC", for: indexPath) as! FoodExtraIngredientsTVC
            cell.btnCheckBox.tag = indexPath.row
            cell.btnCheckBox.addTarget(self, action: #selector(btnCheckBox), for: .touchUpInside)
            if SelectedExtraIngredients.contains(indexPath.row)
            {
                cell.btnCheckBox.setImage(#imageLiteral(resourceName: "Check box"), for: .normal)
            }
            else
            {
                cell.btnCheckBox.setImage(#imageLiteral(resourceName: "Un checked box"), for: .normal)
            }
            cell.lblTitle.text = "Extra Ingredients"
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FoodExtraIngredientsTVC", for: indexPath) as! FoodExtraIngredientsTVC
            
            cell.lblTitle.text = SubscriptionTypeArray[indexPath.row]
            
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    
}
