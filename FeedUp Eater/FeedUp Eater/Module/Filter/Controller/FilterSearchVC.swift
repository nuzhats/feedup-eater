//
//  FilterSearchVC.swift
//  FeedUp Eater
//
//  Created by Variance on 24/09/21.
//

import UIKit

class FilterSearchVC: UIViewController {
    
    //MARK:- Outlets --
    @IBOutlet weak var tblList: UITableView!
    
    //MARK:- Varibles --
    let FoodYouLoveArray = [["image":#imageLiteral(resourceName: "Salad"),"name":"Salad"],["image":#imageLiteral(resourceName: "Dessert"),"name":"Dessert"],["image":#imageLiteral(resourceName: "Pizza"),"name":"Pizza"],["image":#imageLiteral(resourceName: "eggs"),"name":"Eggs"]]
    var SelectedInddex = [Int]()
    
    //MARK:- View Life Cycle --
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    
    // MARK: - Button Action Method --
    @IBAction func btnFilter(_ sender: Any)
    {
    }
    
    @IBAction func btnDelivery(_ sender: Any)
    {
    }
    
    @IBAction func btnPickUp(_ sender: Any)
    {
    }
    
    @IBAction func btnNotification(_ sender: Any)
    {
        let VC: NotificationListVC = Utilities.viewController(name: "NotificationListVC", onStoryBoared: StoryBoaredNames.profile.rawValue) as! NotificationListVC
        VC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(VC, animated: true)
    }
    


}
//MARK:- Tableview Delegate & Datasource Method --
extension FilterSearchVC : UITableViewDelegate, UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 1
        }
        else if section == 1
        {
            return 2
        }
        else if section == 2
        {
            return 1
        }
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterSearchTextFieldTVC", for: indexPath) as! FilterSearchTextFieldTVC
            
            
            return cell
        }
        else if indexPath.section == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RecentlySearchTVC", for: indexPath) as! RecentlySearchTVC
            if indexPath.row == 0
            {
                cell.lblSearchTitle.text = "KFC"
            }
            else
            {
                cell.lblSearchTitle.text = "The Foodie Thing"
            }
            
            return cell
        }
        else if indexPath.section == 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FoodYouLoveTVC", for: indexPath) as! FoodYouLoveTVC
            
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RecentlySearchRestorentListTVC", for: indexPath) as! RecentlySearchRestorentListTVC
            
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    private func tableView(tableView: UITableView,
                           willDisplayCell cell: UITableViewCell,
                           forRowAtIndexPath indexPath: NSIndexPath)
    {
        
        
        guard let tableViewCell = cell as? FoodYouLoveTVC else { return }
                    
        tableViewCell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forRow: indexPath.row, forSection: indexPath.section)
             
    }
    
    private func tableView(tableView: UITableView,
                           didEndDisplayingCell cell: UITableViewCell,
                           forRowAtIndexPath indexPath: NSIndexPath)
    {
        
        guard cell is FoodYouLoveTVC else { return }
       
    }
    
}
//MARK:- Collectionviee Datasource & Delegate
extension FilterSearchVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return FoodYouLoveArray.count
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
       
        let cell: FoodYouLoveCVC = collectionView.dequeueReusableCell(withReuseIdentifier: "FoodYouLoveCVC", for: indexPath) as! FoodYouLoveCVC
        let Obj = FoodYouLoveArray[indexPath.row]
        if SelectedInddex.contains(indexPath.row)
        {
            cell.viewHolder.backgroundColor = themeColor
            cell.lblName.textColor = .white
        }
        else
        {
            cell.viewHolder.backgroundColor = CostomeLightSelectedColor
            cell.lblName.textColor = DarkUnSelectedColor
        }
        cell.imgFood.image = Obj["image"] as? UIImage
        cell.lblName.text = Obj["name"] as? String
        
        return cell
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if let index = SelectedInddex.firstIndex(where: {$0 == indexPath.row})
        {
            SelectedInddex.remove(at: index)
        }
        else
        {
            SelectedInddex.append(indexPath.row)
        }
        //collectionView.reloadData()
        let indexPath = IndexPath(item: indexPath.row, section: 0)
        collectionView.reloadItems(at: [indexPath])
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        //Set cell daynamic width accroding module name
        let Obj = FoodYouLoveArray[indexPath.row]
        let label = UILabel(frame: CGRect.zero)
        label.font = UIFont(name: "SourceSansPro-Regular", size: 15.0)
        label.text = Obj["name"] as? String
        label.sizeToFit()
        return CGSize(width: label.frame.width + 75, height: 40)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }

    
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
   
}
