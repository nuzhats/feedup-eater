//
//  FoodExtraIngredientsTVC.swift
//  FeedUp Eater
//
//  Created by Variance on 25/09/21.
//

import UIKit

class FoodExtraIngredientsTVC: UITableViewCell {

    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var constBtnCheckBoxWidth: NSLayoutConstraint!
    @IBOutlet weak var constlblTitleTrailing: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
