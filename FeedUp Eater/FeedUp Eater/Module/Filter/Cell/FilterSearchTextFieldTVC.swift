//
//  FilterSearchTextFieldTVC.swift
//  FeedUp Eater
//
//  Created by Variance on 24/09/21.
//

import UIKit

class FilterSearchTextFieldTVC: UITableViewCell {

    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnShowMore: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
