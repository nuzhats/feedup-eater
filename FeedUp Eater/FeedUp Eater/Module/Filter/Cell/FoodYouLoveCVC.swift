//
//  FoodYouLoveCVC.swift
//  FeedUp Eater
//
//  Created by Variance on 24/09/21.
//

import UIKit

class FoodYouLoveCVC: UICollectionViewCell {
    
    @IBOutlet weak var viewHolder: GradientView!
    @IBOutlet weak var imgFood: UIImageView!
    @IBOutlet weak var lblName: UILabel!
}
