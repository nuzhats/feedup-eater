//
//  RecentlySearchRestorentListTVC.swift
//  FeedUp Eater
//
//  Created by Variance on 25/09/21.
//

import UIKit

class RecentlySearchRestorentListTVC: UITableViewCell {

    @IBOutlet weak var imgResturent: UIImageView!
    @IBOutlet weak var lblResturentName: UILabel!
    @IBOutlet weak var lblDetails: UILabel!
    @IBOutlet weak var imgVegNonVeg: UIImageView!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
