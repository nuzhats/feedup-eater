//
//  DetailFoodListTVC.swift
//  FeedUp Eater
//
//  Created by Variance on 25/09/21.
//

import UIKit

class DetailFoodListTVC: UITableViewCell {

    @IBOutlet weak var imgFood: UIImageView!
    @IBOutlet weak var lblFoodName: UILabel!
    @IBOutlet weak var lblFoodDetails: UILabel!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var viewAddMore: GradientView!
    @IBOutlet weak var viewAdd: GradientView!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var btnPlus: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
