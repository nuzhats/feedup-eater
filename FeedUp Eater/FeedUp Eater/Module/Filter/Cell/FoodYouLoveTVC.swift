//
//  FoodYouLoveTVC.swift
//  FeedUp Eater
//
//  Created by Variance on 24/09/21.
//

import UIKit

class FoodYouLoveTVC: UITableViewCell {

    @IBOutlet weak var lblOlderCategory: UILabel!
    @IBOutlet weak var lblFoodYouLove: UILabel!
    @IBOutlet weak var cvFoodYouLove: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCollectionViewDataSourceDelegate(dataSourceDelegate: UICollectionViewDataSource & UICollectionViewDelegate, forRow row: Int, forSection section: Int) {
        cvFoodYouLove.delegate = dataSourceDelegate
        cvFoodYouLove.dataSource = dataSourceDelegate
        /* to get section for the collection view multiply it with any tens and add row in that. So that at the time of data fetching I can easily get section and row number by deviding tag to its tens and by moduling tag to that tens. On the bases of section I can get data from the array. Here I use 100000 as a multiplier*/
        
        cvFoodYouLove.reloadData()
    }

}
