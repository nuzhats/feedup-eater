//
//  TabBarVC.swift
//  FeedUp Eater
//
//  Created by Variance on 22/09/21.
//

import UIKit

class TabBarVC: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.delegate = self
    }
    

    // called whenever a tab button is tapped
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        print("Selected item")
        if tabBar.items != nil
        {
            for obj in tabBar.items!
            {
                if obj == item
                {
                    if item.tag == 0
                    {
                        obj.title = "Home"
                    }
                    else if item.tag == 1
                    {
                        obj.title = "Orders"
                    }
                    else if item.tag == 2
                    {
                        obj.title = "Cart"
                    }
                    else if item.tag == 3
                    {
                        obj.title = "Profile"
                    }
                    else
                    {
                       obj.title = ""
                    }
                }
                else
                {
                   obj.title = ""
                }
            }
        }
    }

}
