//
//  DeliveryOptionModel.swift
//  FeedUp Eater
//
//  Created by Variance on 26/09/21.
//

import Foundation
import SwiftyJSON

class DeliveryOptionModel : BaseModel {
    
    var DeliveryOption = [String]()
    var DeliveryOptionTitle: String = ""
    var placeHolder:String = ""
    var SelectedIndex = -1
}
