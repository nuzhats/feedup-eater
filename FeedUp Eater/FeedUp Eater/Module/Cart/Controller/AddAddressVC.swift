//
//  AddAddressVC.swift
//  FeedUp Eater
//
//  Created by Variance on 26/09/21.
//

import UIKit
import GoogleMaps

class AddAddressVC: UIViewController {

    //MARK:- Outlets --
    @IBOutlet var viewMap: GMSMapView!
    @IBOutlet weak var lblCurrentAddress: UILabel!
    @IBOutlet weak var tvAddAddress: TextViewWithPlaceholder!
    @IBOutlet weak var txtLandmark: UITextField!
    @IBOutlet weak var tblDeliveryOption: UITableView!
    @IBOutlet weak var btnAddAddress: UIButton!
    @IBOutlet weak var constTblHeight: NSLayoutConstraint!
    
    //MARK:- Varibles --
    var DeliveryOption = [DeliveryOptionModel]()
    var OptionType = [Leave_at_door,Meet_at_door,Meet_outside]
    
    var isEditAddress = false
    
    //MARK:- View Life Cycle --
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if isEditAddress
        {
            btnAddAddress.setTitle(Btn_Update, for: .normal)
        }
        else
        {
            btnAddAddress.setTitle(Btn_Add_Address, for: .normal)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        GetLocation()
        for i in 0..<2
        {
            let obj = DeliveryOptionModel()
            if i == 0
            {
                obj.DeliveryOption = OptionType
                obj.placeHolder = Add_Instruction
            }
            else
            {
                obj.placeHolder = Add_Label
            }
            DeliveryOption.append(obj)
        }
        ManageTabaleHeight()
    }
    //MARK:- Manage Table Height
    func ManageTabaleHeight()
    {
        var WithCVCount = 0
        var WithOutCVCount = 0
        if DeliveryOption.first(where: {$0.DeliveryOption.count > 0}) != nil
        {
            WithCVCount += 1
        }
        WithOutCVCount = DeliveryOption.count - WithCVCount
        let tblHeight = (CGFloat(WithCVCount * 195)) + (CGFloat(WithOutCVCount * 130))
        constTblHeight.constant = tblHeight//Need To change dynamic once get api
    }
    
    //MARK:- Get Current Location
    func GetLocation()
    {
        LocationManager.sharedInstance.getLocation(completionHandler: { (location, error) in
            
            if location != nil
            {
                let latitude = location!.coordinate.latitude
                let longitude = location!.coordinate.longitude
                let marker = GMSMarker()
                marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                marker.icon = #imageLiteral(resourceName: "delivery location")
                marker.title = ""
                marker.snippet = ""
                
                marker.map = self.viewMap
                self.ZoomOnLatLong(lat: latitude, Long: longitude, Zoom: 7.0)
            }
            else if error != nil
            {
                
            }
        })
    }
    //MARK:- Zoom on Lat Long
    func ZoomOnLatLong(lat: Double, Long: Double, Zoom: Float)
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(01), execute: {
           let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: Long, zoom: Zoom)

           self.viewMap?.animate(to: camera)
       })
    }

    @IBAction func btnAddAddress(_ sender: Any) {
    }
    

}
//MARK:- Tableview Delegate & Datasource Method --
extension AddAddressVC : UITableViewDelegate, UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return DeliveryOption.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DeliveryOptionTVC", for: indexPath) as! DeliveryOptionTVC
        let Obj = DeliveryOption[indexPath.row]
        cell.lblDeliveryOption.text = Delivery_Option + " \(indexPath.row + 1)"
        cell.txtAddInstruction.placeholder = Obj.placeHolder
        if Obj.DeliveryOption.count > 0
        {
            cell.constCVHeight.constant = 45
            cell.constCVTop.constant = 20
            cell.CVDeliveryOption.isHidden = false
        }
        else
        {
            cell.constCVHeight.constant = 0
            cell.constCVTop.constant = 0
            cell.CVDeliveryOption.isHidden = true
        }
        cell.CVDeliveryOption.tag = indexPath.row
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    private func tableView(tableView: UITableView,
                           willDisplayCell cell: UITableViewCell,
                           forRowAtIndexPath indexPath: NSIndexPath)
    {
        
        
        guard let tableViewCell = cell as? FoodYouLoveTVC else { return }
                    
        tableViewCell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forRow: indexPath.row, forSection: indexPath.section)
             
    }
    
    private func tableView(tableView: UITableView,
                           didEndDisplayingCell cell: UITableViewCell,
                           forRowAtIndexPath indexPath: NSIndexPath)
    {
        
        guard cell is FoodYouLoveTVC else { return }
       
    }
    
    
}
//MARK:- Collectionviee Datasource & Delegate
extension AddAddressVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        
        return DeliveryOption[collectionView.tag].DeliveryOption.count
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
       
        let cell: FoodYouLoveCVC = collectionView.dequeueReusableCell(withReuseIdentifier: "FoodYouLoveCVC", for: indexPath) as! FoodYouLoveCVC
        let Obj = DeliveryOption[collectionView.tag]
        cell.lblName.text = Obj.DeliveryOption[indexPath.row]
        if Obj.SelectedIndex == indexPath.row
       {
           cell.viewHolder.backgroundColor = themeColor
           cell.lblName.textColor = .white
       }
       else
       {
           cell.viewHolder.backgroundColor = CostomeLightSelectedColor
           cell.lblName.textColor = DarkUnSelectedColor
       }
        return cell
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let Obj = DeliveryOption[collectionView.tag]
        /*var IndexPathArr = [IndexPath]()
        let SelectedIndex = IndexPath(item: indexPath.row, section: 0)
        IndexPathArr.append(SelectedIndex)
        if Obj.SelectedIndex != -1
        {
            let LastSelected = IndexPath(item: indexPath.row, section: 0)
            IndexPathArr.append(LastSelected)
        }*/
        Obj.SelectedIndex = indexPath.row
        collectionView.reloadData()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        //Set cell daynamic width accroding module name
        let Obj = DeliveryOption[collectionView.tag].DeliveryOption[indexPath.row]
        let label = UILabel(frame: CGRect.zero)
        label.font = UIFont(name: "SourceSansPro-Semibold", size: 16.0)
        label.text = Obj
        label.sizeToFit()
        return CGSize(width: label.frame.width + 30, height: 40)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }

    
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
   
}
