//
//  CartVC.swift
//  FeedUp Eater
//
//  Created by Variance on 22/09/21.
//

import UIKit
import MapKit
import GoogleMaps
import Alamofire
import SwiftyJSON

class CartVC: UIViewController {
    
    //MARK:- Outlets --
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnAddAddress: UIButton!
    @IBOutlet weak var tblFoodList: UITableView!
    @IBOutlet weak var btnAddItems: UIButton!
    @IBOutlet weak var txtGiftName: UITextField!
    @IBOutlet weak var tvGiftMessage: TextViewWithPlaceholder!
    
    @IBOutlet weak var lblSubTotalPrice: UILabel!
    @IBOutlet weak var lblDeliveryChargePrice: UILabel!
    @IBOutlet weak var lblTaxesPrice: UILabel!
    @IBOutlet weak var lblPromotionsPrice: UILabel!
    @IBOutlet weak var lblDiscountPercentage: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var constTblFoodListHeight: NSLayoutConstraint!
    @IBOutlet weak var btnMakeAGift: UIButton!
    @IBOutlet weak var viewAddGiftDetail: UIView!
    @IBOutlet weak var btnApplayCouponCode: UIButton!
    @IBOutlet weak var viewApplayCouponCode: UIView!
    @IBOutlet weak var txtApplayCouponCode: UITextField!
    @IBOutlet weak var viewAddTip: UIView!
    @IBOutlet weak var btnAddTip: UIButton!
    @IBOutlet weak var txtAddTip: UITextField!
    
    //MARK:- Varibles --
    var AddData : [RestaurantDetailModel] = [RestaurantDetailModel]()
    var latitude = Double()
    var longitude = Double()
    
    //MARK:- View Life Cycle --
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        btnAddAddress.underlineButtonText(textColor: themeColor)
        btnAddItems.underlineButtonText(textColor: themeColor)
        for i in 0..<2
        {
            let obj = RestaurantDetailModel()
            obj.indexInt = i
            obj.Count = 1
            AddData.append(obj)
        }
        ManageTabaleHeight()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        GetLocation()
        self.tabBarController?.tabBar.isHidden = false
        
    }
    //MARK:- Manage Table Height
    func ManageTabaleHeight()
    {
        constTblFoodListHeight.constant = CGFloat(AddData.count * 45)//Change AddData.count to array count
    }
    //MARK:- Get Current Location
    func GetLocation()
    {
        LocationManager.sharedInstance.getLocation(completionHandler: { (location, error) in
            
            if location != nil
            {
                self.latitude = location!.coordinate.latitude
                self.longitude = location!.coordinate.longitude
                let marker = GMSMarker()
                marker.position = CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
                marker.icon = #imageLiteral(resourceName: "delivery location")
                marker.title = ""
                marker.snippet = ""
                
                marker.map = self.mapView
                self.ZoomOnLatLong(lat: self.latitude, Long: self.longitude, Zoom: 7.0)
               // let destinationCordinate = CLLocationCoordinate2D(latitude: 23.0399, longitude: 72.5543)
               // self.drawMap(SourceCordinate: location!.coordinate, destinationcordinate: destinationCordinate)
            }
            else if error != nil
            {
                
            }
        })
    }
    //MARK:- Zoom on Lat Long
    func ZoomOnLatLong(lat: Double, Long: Double, Zoom: Float)
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(01), execute: {
           let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: Long, zoom: Zoom)

           self.mapView?.animate(to: camera)
       })
    }
    func drawMap(SourceCordinate : CLLocationCoordinate2D, destinationcordinate :CLLocationCoordinate2D)
        {
            self.mapView.clear()
            let str = String(format:"\(GoogleMapDirectionsBaseUrl)\(SourceCordinate.latitude),\(SourceCordinate.longitude)&destination=\(destinationcordinate.latitude),\(destinationcordinate.longitude)&key=\(GoogleMapKey)")
            print(str)
            Alamofire.request(str).responseJSON { (responseObject) -> Void in
                let resJson = JSON(responseObject.result.value!)
                print(resJson)
                let routes : NSArray = resJson["routes"].rawValue as! NSArray
                if(resJson["status"].rawString()! == "ZERO_RESULTS"){}
                else if(resJson["status"].rawString()! == "NOT_FOUND"){}
                else if routes.count == 0{}
                else{
                    let routes : NSArray = resJson["routes"].rawValue as! NSArray
    //                let position = CLLocationCoordinate2D(latitude: SourceCordinate.latitude, longitude: SourceCordinate.longitude)
                    self.DrawPolyLine(routesArr: routes)

                }
            }
        }
    
    func DrawPolyLine(routesArr: NSArray)
    {
        let markerEnd = GMSMarker()
        markerEnd.position = CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
        markerEnd.map = self.mapView
        let pathv : NSArray = routesArr.value(forKey: "overview_polyline") as! NSArray
        let paths : NSArray = pathv.value(forKey: "points") as! NSArray
        let newPath = GMSPath.init(fromEncodedPath: paths[0] as! String)
        let polyLine = GMSPolyline(path: newPath)
        polyLine.strokeWidth = 5
        polyLine.strokeColor =  .black
        let ThemeOrange = GMSStrokeStyle.solidColor( .blue)
        let OrangeToBlue = GMSStrokeStyle.gradient(from:  .blue, to:  .blue)
        polyLine.spans = [GMSStyleSpan(style: ThemeOrange),
                          GMSStyleSpan(style: ThemeOrange),
                          GMSStyleSpan(style: OrangeToBlue)]
        polyLine.map = self.mapView
    }
    
    //MARK:- Button Action Method --
    @IBAction func btnEditAddress(_ sender: Any)
    {
        let VC: AddAddressVC = Utilities.viewController(name: "AddAddressVC", onStoryBoared: StoryBoaredNames.cart.rawValue) as! AddAddressVC
        VC.isEditAddress = true
        VC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func btnAddAddress(_ sender: Any)
    {
        let VC: AddAddressVC = Utilities.viewController(name: "AddAddressVC", onStoryBoared: StoryBoaredNames.cart.rawValue) as! AddAddressVC
        VC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func btnAddItems(_ sender: Any)
    {
        
    }
    
    @IBAction func btnMakeAGift(_ sender: Any)
    {
        if btnMakeAGift.isSelected
        {
            btnMakeAGift.setImage(#imageLiteral(resourceName: "Expand"), for: .normal)
            viewAddGiftDetail.isHidden = true
            btnMakeAGift.isSelected = false
        }
        else
        {
            btnMakeAGift.setImage(#imageLiteral(resourceName: "Collapse"), for: .normal)
            viewAddGiftDetail.isHidden = false
            btnMakeAGift.isSelected = true
        }
    }
    @IBAction func btnAddTip(_ sender: Any)
    {
        if btnAddTip.isSelected
        {
            btnAddTip.setImage(#imageLiteral(resourceName: "Expand"), for: .normal)
            viewAddTip.isHidden = true
            btnAddTip.isSelected = false
        }
        else
        {
            btnAddTip.setImage(#imageLiteral(resourceName: "Collapse"), for: .normal)
            viewAddTip.isHidden = false
            btnAddTip.isSelected = true
        }
    }
    @IBAction func btnApplayCouponCode(_ sender: Any)
    {
        if btnApplayCouponCode.isSelected
        {
            btnApplayCouponCode.setImage(#imageLiteral(resourceName: "Expand"), for: .normal)
            viewApplayCouponCode.isHidden = true
            btnApplayCouponCode.isSelected = false
        }
        else
        {
            btnApplayCouponCode.setImage(#imageLiteral(resourceName: "Collapse"), for: .normal)
            viewApplayCouponCode.isHidden = false
            btnApplayCouponCode.isSelected = true
        }
    }
    @IBAction func btnMakePayment(_ sender: Any)
    {
        OrpderPlacePopUp()
    }
    //MARK:- popup --
    func OrpderPlacePopUp()
    {
        self.tabBarController?.tabBar.isHidden = true
        let vc = Utilities.viewController(name: "CustomeSuccessPopUpVC", onStoryBoared: StoryBoaredNames.authentication.rawValue) as! CustomeSuccessPopUpVC
        vc.popup = popUp.OrderPlaced
        
        
        vc.completionHandler = {
            (type: Int) -> () in
            
            if type == 0//cancel button
            {
                vc.dismiss(animated: true, completion: nil)
                // perform cancel action
                self.tabBarController?.tabBar.isHidden = false
                let VC: OrderTrackingVC = Utilities.viewController(name: "OrderTrackingVC", onStoryBoared: StoryBoaredNames.cart.rawValue) as! OrderTrackingVC
                VC.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(VC, animated: true)
            }
            else
            {
                vc.dismiss(animated: true, completion: nil)
            }
        }
        self.present(vc, animated: true, completion: nil)
    }
    //MARK:- Cell Button Action Method --
    @objc func btnMinus(sender: UIButton)
    {
        var isReloadFulltableview = false
        if let index = AddData.firstIndex(where: {$0.indexInt == sender.tag})
        {
            AddData[index].Count -= 1
            if AddData[index].Count == 0
            {
                AddData.remove(at: index)
                isReloadFulltableview = true
                for obj in AddData
                {
                    obj.indexInt -= 1
                }
                tblFoodList.reloadData()
            }
        }
        if !isReloadFulltableview
        {
            ReloadIndex(ind: sender.tag)
        }
        else
        {
            ManageTabaleHeight()
        }
    }
    @objc func btnPlus(sender: UIButton)
    {
        if let index = AddData.firstIndex(where: {$0.indexInt == sender.tag})
        {
            AddData[index].Count += 1
        }
        ReloadIndex(ind: sender.tag)
    }
    //MARK:- Reload Index Path --
    func ReloadIndex(ind: Int)
    {
        let indexPath = IndexPath(row: ind, section: 0)
        tblFoodList.reloadRows(at: [indexPath], with: .automatic)
        
    }

}
//MARK:- Tableview Delegate & Datasource Method --
extension CartVC : UITableViewDelegate, UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return AddData.count
    }
    //
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartFoodListTVC", for: indexPath) as! CartFoodListTVC
        
        cell.btnMinus.tag = indexPath.row
        cell.btnMinus.addTarget(self, action: #selector(btnMinus), for: .touchUpInside)
        
        cell.btnPlus.tag = indexPath.row
        cell.btnPlus.addTarget(self, action: #selector(btnPlus), for: .touchUpInside)
        
        if let ind = AddData.firstIndex(where: {$0.indexInt == indexPath.row})
        {
            cell.lblQuantity.text = "\(AddData[ind].Count)"
            
        }
        else
        {
            cell.lblQuantity.text = "0"
        }
        
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
       
    }
    
    
}
