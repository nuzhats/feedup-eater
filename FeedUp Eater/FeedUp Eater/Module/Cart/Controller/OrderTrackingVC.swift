//
//  OrderTrackingVC.swift
//  FeedUp Eater
//
//  Created by Variance on 27/09/21.
//

import UIKit
import GoogleMaps

class OrderTrackingVC: UIViewController {
    
    //MARK:- Outlets --
    @IBOutlet var viewMap: GMSMapView!
    @IBOutlet weak var lblEastimatedTime: UILabel!
    @IBOutlet weak var tblOrderTracking: UITableView!
    @IBOutlet weak var constTblOrderTrackingHeight: NSLayoutConstraint!
    @IBOutlet weak var imgDriver: UIImageView!
    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet weak var lblOrderDetal: UILabel!
    @IBOutlet weak var tblFoodList: UITableView!
    @IBOutlet weak var constTblFoodListHeight: NSLayoutConstraint!
    @IBOutlet weak var lblTotalPrice: UILabel!
    
    //MARK:- Varibles --
    var TrackingData = [OrderTrackingModel]()
    var TrackingTitleArray =   [Order_Accepted,Food_prepared,Food_on_way,Order_arrived]
    var OrderTblCount = 2
    
    //MARK:- View Life Cycle --
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        for obj in TrackingTitleArray
        {
            let data = OrderTrackingModel()
            data.TrackingTitle = obj
            if obj == Order_Accepted
            {
                data.Status = true
                data.ComplatedImage = #imageLiteral(resourceName: "Schedule")
                data.NoralImage = #imageLiteral(resourceName: "Order Accepted01")
            }
            else if obj == Food_prepared
            {
                data.Status = true
                data.ComplatedImage = #imageLiteral(resourceName: "Food Prepareed")
                data.NoralImage = #imageLiteral(resourceName: "Food Prepared01")
            }
            else if obj == Food_on_way
            {
                data.Status = false
                data.ComplatedImage = #imageLiteral(resourceName: "Food on the way")
                data.NoralImage = #imageLiteral(resourceName: "Food on the way01")
            }
            else
            {
                data.Status = false
                data.ComplatedImage = #imageLiteral(resourceName: "Order arrived")
                data.NoralImage =  #imageLiteral(resourceName: "Order arrived01")
            }
            TrackingData.append(data)
            
        }
        ManageTabaleHeight()
    }
    //MARK:- Manage Table Height
    func ManageTabaleHeight()
    {
        constTblOrderTrackingHeight.constant = CGFloat(3 * 79) + 55.0//Change TblExtraIngredientsCount to array count
        constTblFoodListHeight.constant = CGFloat(OrderTblCount * 45)
    }

    //MARK:- Button Action Method --
    @IBAction func btnCall(_ sender: Any)
    {
        Utilities.dialNumber(number: "1234567890")
    }
    
    @IBAction func btnMessage(_ sender: Any)
    {
        let vcPopUP: ChatVC = Utilities.viewController(name: "ChatVC", onStoryBoared: StoryBoaredNames.cart.rawValue) as! ChatVC
        vcPopUP.modalPresentationStyle = .overFullScreen
        self.present(vcPopUP, animated: true, completion: nil)
    }
    

}
//MARK:- Tableview Delegate & Datasource Method --
extension OrderTrackingVC : UITableViewDelegate, UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == tblOrderTracking
        {
            return TrackingData.count
        }
        return OrderTblCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == tblOrderTracking
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderTrackingTVC", for: indexPath) as! OrderTrackingTVC
            let Obj = TrackingData[indexPath.row]
            cell.lblTitle.text = Obj.TrackingTitle
            if Obj.Status
            {
                cell.lblTitle.textColor = themeColor
                cell.viewTrackingLine.backgroundColor = themeColor
                cell.imgStatus.image = Obj.ComplatedImage
            }
            else
            {
                cell.lblTitle.textColor = orderTrackigLightColor
                
                
                cell.viewTrackingLine.backgroundColor = orderTrackigLightColor
                
                cell.imgStatus.image = Obj.NoralImage
            }
            if indexPath.row == TrackingData.count - 1
            {
                cell.constTrackingLineHeight.constant = 0
                cell.viewTrackingLine.isHidden = true
            }
            else
            {
                cell.viewTrackingLine.isHidden = false
                cell.constTrackingLineHeight.constant = 38
            }
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CartFoodListTVC", for: indexPath) as! CartFoodListTVC
            
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    
}
