//
//  ChatVC.swift
//  FeedUp Eater
//
//  Created by Jayesh Godhaniya on 28/09/21.
//

import UIKit

class ChatVC: UIViewController {

    //MARK:- Outlets --
    @IBOutlet var txtMessage: UITextField!
    @IBOutlet var imgDriverImage: UIImageView!
    @IBOutlet var lblDriverName: UILabel!
    @IBOutlet var lblVehicleDetails: UILabel!
    @IBOutlet var lblVehicleNumer: UILabel!
    
    //MARK:- View Life Cycle --
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    //MARK:- View TuchEvent --
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        let touch = touches.first
        if touch?.view == self.view
        {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    //MARK:- Button Action Method --
    @IBAction func btnCall(_ sender: Any)
    {
        Utilities.dialNumber(number: "1234567890")
    }
    

}
