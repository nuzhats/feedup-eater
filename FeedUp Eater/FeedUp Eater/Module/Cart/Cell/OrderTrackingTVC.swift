//
//  OrderTrackingTVC.swift
//  FeedUp Eater
//
//  Created by Variance on 27/09/21.
//

import UIKit

class OrderTrackingTVC: UITableViewCell {

    @IBOutlet weak var imgStatus: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewTrackingLine: UIView!
    @IBOutlet weak var constTrackingLineHeight: NSLayoutConstraint!
    @IBOutlet weak var viewStackLine: UIStackView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
