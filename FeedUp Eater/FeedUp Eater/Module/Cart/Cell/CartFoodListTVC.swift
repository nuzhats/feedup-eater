//
//  CartFoodListTVC.swift
//  FeedUp Eater
//
//  Created by Variance on 26/09/21.
//

import UIKit

class CartFoodListTVC: UITableViewCell {

    @IBOutlet weak var imgVegNonVeg: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblFoodName: UILabel!
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var lblQuantity: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
