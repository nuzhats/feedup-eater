//
//  SearchFoodVC.swift
//  FeedUp Eater
//
//  Created by Nitesh keshvala on 9/29/21.
//

import UIKit

class SearchFoodVC: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var searchFoodColletion: UICollectionView!
    var searchHeaderView:searchCollectionReusableView?

    override func viewDidLoad() {
        super.viewDidLoad()
        searchFoodColletion.dataSource = self
        searchFoodColletion.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //self.tabBarController?.tabBar.isHidden = false
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSize(width:(searchFoodColletion.frame.size.width-20)/2, height: (searchFoodColletion.frame.size.width-20)/2)
       // flowLayout.sectionInset = UIEdgeInsets(top: 15, left: 5, bottom: 15, right: 5)
        flowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
        flowLayout.minimumInteritemSpacing = 10
        flowLayout.minimumLineSpacing = 20
        
        flowLayout.headerReferenceSize = CGSize(width: searchFoodColletion.frame.size.width, height: 150)
        searchFoodColletion.collectionViewLayout = flowLayout
    }
    
    //MARK:- Button Action Method --
    @IBAction func btnFilter(_ sender: Any)
    {
        let VC: SortAndFilterVC = Utilities.viewController(name: "SortAndFilterVC", onStoryBoared: StoryBoaredNames.filter.rawValue) as! SortAndFilterVC
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func btnDelivery(_ sender: Any)
    {
    }
    
    @IBAction func btnPickUp(_ sender: Any)
    {
    }
    
    @IBAction func btnNotification(_ sender: Any)
    {
        let VC: NotificationListVC = Utilities.viewController(name: "NotificationListVC", onStoryBoared: StoryBoaredNames.profile.rawValue) as! NotificationListVC
        VC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(VC, animated: true)
    }
    @IBAction func btnSearch(_ sender: Any)
    {
        let VC: FilterSearchVC = Utilities.viewController(name: "FilterSearchVC", onStoryBoared: StoryBoaredNames.filter.rawValue) as! FilterSearchVC
        VC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "searchHeaderView", for: indexPath) as! searchCollectionReusableView
            self.searchHeaderView = headerView
            
            return headerView
        default:
        return UICollectionReusableView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchFoodCell", for: indexPath) as! SearchFoodCell
            cell.foodImg.image = UIImage(named:"Food image02")
            cell.foodtypelbl.text = "Italian"
            return cell
    }
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        let VC: RestaurantDetailVC = Utilities.viewController(name: "RestaurantDetailVC", onStoryBoared: StoryBoaredNames.filter.rawValue) as! RestaurantDetailVC
        VC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(VC, animated: true)
    }

}
class searchCollectionReusableView: UICollectionReusableView {
        
    //@IBOutlet weak var collectionView: UICollectionView!

}
