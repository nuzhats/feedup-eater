//
//  HomeVC.swift
//  FeedUp Eater
//
//  Created by Variance on 22/09/21.
//

import UIKit

var selectedCatagryIndexPath = 0

class HomeVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var hotelListColletion: UICollectionView!
    
    //MARK:- Varibles --
    let FoodYouLoveArray = [["image":#imageLiteral(resourceName: "Salad"),"name":"Salad"],["image":#imageLiteral(resourceName: "Dessert"),"name":"Dessert"],["image":#imageLiteral(resourceName: "Pizza"),"name":"Pizza"],["image":#imageLiteral(resourceName: "eggs"),"name":"Eggs"]]
    var headerView:CustomCollectionReusableView?
    var lastSelectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        hotelListColletion.dataSource = self
        hotelListColletion.delegate = self
        SetUpView()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        
    }
    func SetUpView()
    {
        let flowLayout = UICollectionViewFlowLayout()
        let HeightWidth = (hotelListColletion.frame.size.width-20)/2
        flowLayout.itemSize = CGSize(width:HeightWidth, height: HeightWidth + 35)
       // flowLayout.sectionInset = UIEdgeInsets(top: 15, left: 5, bottom: 15, right: 5)
        flowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
        flowLayout.minimumInteritemSpacing = 10
        flowLayout.minimumLineSpacing = 20

        flowLayout.headerReferenceSize = CGSize(width: hotelListColletion.frame.size.width, height: 220)
        hotelListColletion.collectionViewLayout = flowLayout
    }
    // MARK: - Button Action Method --
    @IBAction func searchButton(sender: UIButton){
      print("You pressed me")
        let VC: SearchFoodVC = Utilities.viewController(name: "SearchFoodVC", onStoryBoared: StoryBoaredNames.home.rawValue) as! SearchFoodVC
        VC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func btnFilter(_ sender: Any)
    {
        let VC: SortAndFilterVC = Utilities.viewController(name: "SortAndFilterVC", onStoryBoared: StoryBoaredNames.filter.rawValue) as! SortAndFilterVC
        VC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func btnDelivery(_ sender: Any)
    {
    }
    
    @IBAction func btnPickUp(_ sender: Any)
    {
    }
    
    @IBAction func btnNotification(_ sender: Any)
    {
        let VC: NotificationListVC = Utilities.viewController(name: "NotificationListVC", onStoryBoared: StoryBoaredNames.profile.rawValue) as! NotificationListVC
        VC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(VC, animated: true)
    }

   /* public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView != self.hotelListColletion{
            let size:CGFloat = (self.view.frame.size.width - 40) / 2.0
            print("size",size)
            
            return CGSize(width: size, height: size)
        }
        else{
            let size:CGFloat = (collectionView.frame.size.width) / 2.0
            return CGSize(width: size, height: 40)
        }
    }*/
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.hotelListColletion{
            return 50
        }else{
            return FoodYouLoveArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath) as! CustomCollectionReusableView
            self.headerView = headerView
            headerView.collectionView.delegate = self
            headerView.collectionView.dataSource = self
            
            return headerView
        default:
        return UICollectionReusableView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           if collectionView == self.hotelListColletion{

            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HotelCell", for: indexPath) as! hotelsListCallectionCell
           // cell.hotelImg.image = UIImage(named:"demoHotel2")
            //cell.hotelImg.contentMode = .scaleAspectFill
           
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeCategoryCell", for: indexPath) as! homeCategoryCell
            let Obj = FoodYouLoveArray[indexPath.row]
            
            cell.logoImg.image = Obj["image"] as? UIImage
            cell.nameLabel.text = Obj["name"] as? String
            if lastSelectedIndex == indexPath.row {
                cell.containerView.backgroundColor = themeColor
                //cell.nameLabel.text = "Selected"
                cell.nameLabel.textColor = .white
            }
            else{
                cell.containerView.backgroundColor = CostomeLightSelectedColor
                cell.nameLabel.textColor = DarkUnSelectedColor
                //cell.nameLabel.text = "Selected"
            }
            return cell
        }
    }
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if collectionView == hotelListColletion
        {
            let VC: RestaurantDetailVC = Utilities.viewController(name: "RestaurantDetailVC", onStoryBoared: StoryBoaredNames.filter.rawValue) as! RestaurantDetailVC
            VC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(VC, animated: true)
        }
        else
        {
            let lastIndexPath = IndexPath(row: lastSelectedIndex, section: indexPath.section)
            lastSelectedIndex = indexPath.row
            //collectionView.reloadItems(at: [indexPath])
            //collectionView.reloadItems(at: [lastIndexPath])
            collectionView.reloadData()
        }
    }
}

class CustomCollectionReusableView: UICollectionReusableView {
        
    @IBOutlet weak var collectionView: UICollectionView!

}
