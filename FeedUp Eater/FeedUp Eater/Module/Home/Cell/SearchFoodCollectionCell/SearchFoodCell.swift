//
//  SearchFoodCell.swift
//  FeedUp Eater
//
//  Created by Nitesh keshvala on 9/29/21.
//

import UIKit

class SearchFoodCell: UICollectionViewCell {
    @IBOutlet weak var foodImg : UIImageView!
    @IBOutlet weak var foodtypelbl : UILabel!
}
