//
//  homeCategoryCell.swift
//  FeedUp Eater
//
//  Created by Nitesh keshvala on 9/25/21.
//

import UIKit

class homeCategoryCell: UICollectionViewCell {
    @IBOutlet weak var containerView : UIView!
    @IBOutlet weak var  nameLabel: UILabel!
    @IBOutlet weak var logoImg : UIImageView!
}
