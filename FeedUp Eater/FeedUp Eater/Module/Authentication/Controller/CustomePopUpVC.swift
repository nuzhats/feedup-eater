//
//  CustomePopUpVC.swift
//  FeedUp Eater
//
//  Created by Variance on 23/09/21.
//

import UIKit

class CustomePopUpVC: UIViewController {
    
    //MARK:- Outlets --
    @IBOutlet weak var viewHolder: UIView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblMessage: UILabel!
    @IBOutlet var btnMainOption: UIButton!
    @IBOutlet var btnSecondOption: UIButton!
    
    //MARK:- Varibles --
    var completionHandler: ((_ type: Int) -> ())?
    var popup = CustomPopUp.LogOut
    
    //MARK:- View Life Cycle --
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupPopupData()
        viewHolder.animateViewWith(direction: .top, offset: 200.0)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupPopupData()
    {
        lblTitle.text = popup.title
        lblMessage.text = popup.msg
        
        btnMainOption.setTitle(popup.mainTitle, for: .normal)
        btnSecondOption.setTitle(popup.otherTitle, for: .normal)
        
        btnSecondOption.isHidden = popup.isSingle
        
    }

    //MARK:- Button Action Methods --
    @IBAction func MainBtnAction(_ sender: UIButton)
    {
        if completionHandler != nil
        {
            self.completionHandler!(0)
        }
    }
    
    @IBAction func SecondBtnAction(_ sender: UIButton)
    {
        if completionHandler != nil
        {
            self.completionHandler!(1)
        }
    }

}
