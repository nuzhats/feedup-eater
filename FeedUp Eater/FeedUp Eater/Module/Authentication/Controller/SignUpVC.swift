//
//  SignUpVC.swift
//  FeedUp Eater
//
//  Created by Jayesh Godhaniya on 21/09/21.
//

import UIKit

class SignUpVC: UIViewController {

    //MARK:- Outlets --
    @IBOutlet weak var lblLogin: UILabel!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfrimPassword: UITextField!
    @IBOutlet weak var btnHideShowPassword: UIButton!
    @IBOutlet weak var btnHideShowConfirmPassword: UIButton!
    
    //MARK:- View Life Cycle --
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        SetUpView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        UIApplication.shared.statusBarUIView?.backgroundColor = BGColor
    }
    
    //MARK:- SetUp View
    func SetUpView()
    {
        lblLogin.underline()
    }

    //MARK:- Button Action Method --
    @IBAction func btnSignUp(_ sender: Any)
    {
        SignUpSuccess()
    }
    
    @IBAction func btnGotoLogin(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnHideShowPassword(_ sender: Any)
    {
        if btnHideShowPassword.isSelected
        {
            txtPassword.isSecureTextEntry = true
            btnHideShowPassword.setImage(#imageLiteral(resourceName: "Show"), for: .normal)
            btnHideShowPassword.isSelected = false
        }
        else
        {
            txtPassword.isSecureTextEntry = false
            btnHideShowPassword.setImage(#imageLiteral(resourceName: "Hide"), for: .normal)
            btnHideShowPassword.isSelected = true
        }
    }
    
    @IBAction func btnHideShowConfirmPassword(_ sender: Any)
    {
        if btnHideShowConfirmPassword.isSelected
        {
            txtConfrimPassword.isSecureTextEntry = true
            btnHideShowConfirmPassword.setImage(#imageLiteral(resourceName: "Show"), for: .normal)
            btnHideShowConfirmPassword.isSelected = false
        }
        else
        {
            txtConfrimPassword.isSecureTextEntry = false
            btnHideShowConfirmPassword.setImage(#imageLiteral(resourceName: "Hide"), for: .normal)
            btnHideShowConfirmPassword.isSelected = true
        }
    }
    
    //MARK:- popup --
    func SignUpSuccess()
    {
        let vc = Utilities.viewController(name: "CustomeSuccessPopUpVC", onStoryBoared: StoryBoaredNames.authentication.rawValue) as! CustomeSuccessPopUpVC
        vc.popup = popUp.AccountCreate
        
        
        vc.completionHandler = {
            (type: Int) -> () in
            
            if type == 0//cancel button
            {
                // perform cancel action
            }
            
            
            vc.dismiss(animated: true, completion: nil)
            
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK:- Validation -
    func validation() -> Bool
    {
        if self.txtName.text?.trim() == ""
        {
            showMessage(message: ENTER_NAME)
            return false
        }
        else if self.txtPhoneNumber.text?.trim() == ""
        {
            showMessage(message: ENTER_PHONE)
            return false
        }
        else if !Utilities.isValidPhone(phone: self.txtPhoneNumber.text!.trim())
        {
            showMessage(message: INVALID_PHONE)
            return false
        }
        else if self.txtEmail.text?.trim() == ""
        {
            showMessage(message: ENTER_EMAIL)
            return false
        }
        else if !Utilities.isValidEmail(testStr: self.txtEmail.text!.trim())
        {
            showMessage(message: INVALID_EMAIL)
            return false
        }
        else if self.txtPassword.text?.trim() == ""
        {
            showMessage(message: ENTER_PASSWORD)
            return false
        }
        else if !Utilities.isValidPassword(testStr: self.txtPassword.text!.trim(), length: 6)
        {
            showMessage(message: INVALID_PASSWORD)
            return false
        }
        else if self.txtConfrimPassword.text?.trim() == ""
        {
            showMessage(message: ENTER_CONFIRMPASSWORD)
            return false
        }
        else if self.txtPassword.text?.trim() != self.txtConfrimPassword.text?.trim()
        {
            showMessage(message: PASSWORD_NOT_MATCH)
            return false
        }
        return true
    }
}
//MARK:- UITextField Delegate --
extension SignUpVC : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == txtName
        {
            txtPhoneNumber.becomeFirstResponder()
        }
        else if textField == txtPhoneNumber
        {
            txtEmail.becomeFirstResponder()
        }
        else if textField == txtEmail
        {
            txtPassword.becomeFirstResponder()
        }
        else if textField == txtPassword
        {
            txtConfrimPassword.becomeFirstResponder()
        }
        else if textField == txtConfrimPassword
        {
            //Sign Up Api Call
            self.view.endEditing(true)
        }
        else
        {
            self.view.endEditing(true)
        }
        return true
    }
}
