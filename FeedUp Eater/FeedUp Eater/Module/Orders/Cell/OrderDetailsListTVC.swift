//
//  OrderDetailsListTVC.swift
//  FeedUp Eater
//
//  Created by Variance on 23/09/21.
//

import UIKit

class OrderDetailsListTVC: UITableViewCell {

    @IBOutlet weak var imgVegNonVeg: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDishName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
