//
//  OrdersListTVC.swift
//  FeedUp Eater
//
//  Created by Jayesh Godhaniya on 22/09/21.
//

import UIKit

class OrdersListTVC: UITableViewCell {

    @IBOutlet weak var lblRestorentName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var imgRestorent: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblOrderDetails: UILabel!
    @IBOutlet weak var lblTimeAndDate: UILabel!
    @IBOutlet weak var btnReorder: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
