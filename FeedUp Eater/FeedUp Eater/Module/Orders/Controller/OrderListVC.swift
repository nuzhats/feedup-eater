//
//  OrderListVC.swift
//  FeedUp Eater
//
//  Created by Jayesh Godhaniya on 22/09/21.
//

import UIKit

class OrderListVC: UIViewController {

    //MARK:- Outlets --
    @IBOutlet weak var tblOrderList: UITableView!
    
    //MARK:- View Life Cycle --
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }

    
    // MARK: - Button Action Method

    

}
//MARK:- Tableview Delegate & Datasource Method --
extension OrderListVC : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrdersListTVC", for: indexPath) as! OrdersListTVC
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let VC: OrderDetailsVC = Utilities.viewController(name: "OrderDetailsVC", onStoryBoared: StoryBoaredNames.Orders.rawValue) as! OrderDetailsVC
        VC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    
}
