//
//  OrderDetailsVC.swift
//  FeedUp Eater
//
//  Created by Jayesh Godhaniya on 22/09/21.
//

import UIKit

class OrderDetailsVC: UIViewController {
    
    //MARK:- Outlets --
    @IBOutlet weak var lblOrderNo: UILabel!
    @IBOutlet weak var lblDeliveredDetails: UILabel!
    @IBOutlet weak var lblPickUpAddress: UILabel!
    @IBOutlet weak var lblDropOffAddress: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDeliveredBy: UILabel!
    @IBOutlet weak var tblOrderList: UITableView!
    @IBOutlet weak var constOrderLisrHolderViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lblDeliveryChargePrice: UILabel!
    @IBOutlet weak var lblTaxesPrice: UILabel!
    @IBOutlet weak var lblPromotionsPrice: UILabel!
    @IBOutlet weak var lblDiscountPercentage: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!
    
    //MARK:- View Life Cycle --
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        SetUpTableHeight()
        
    }
    //MARK:- SetUp Table Height--
    func SetUpTableHeight()
    {
        //Set Table Height accroding dish list
        constOrderLisrHolderViewHeight.constant = 45 * 2
    }

    // MARK: - Button Action Method --
    @IBAction func btnReorder(_ sender: Any)
    {
        let vcPopUP: AddReviewVC = Utilities.viewController(name: "AddReviewVC", onStoryBoared: StoryBoaredNames.Orders.rawValue) as! AddReviewVC
        vcPopUP.modalPresentationStyle = .overFullScreen
        self.present(vcPopUP, animated: true, completion: nil)
    }
    
    

}
//MARK:- Tableview Delegate & Datasource Method --
extension OrderDetailsVC : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailsListTVC", for: indexPath) as! OrderDetailsListTVC
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    
}
