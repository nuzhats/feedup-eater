//
//  AddReviewVC.swift
//  FeedUp Eater
//
//  Created by Jayesh Godhaniya on 28/09/21.
//

import UIKit

class AddReviewVC: UIViewController {

    //MARK:- Outlets --
    @IBOutlet var imgMainImage: UIImageView!
    @IBOutlet var imgSecondryImage: UIImageView!
    @IBOutlet var btnSkip: UIButton!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblTaste: UILabel!
    @IBOutlet var lblPackaging: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var viewReviewTaste: FloatRatingView!
    @IBOutlet var viewReviewPackaging: FloatRatingView!
    @IBOutlet var viewReviewPrice: FloatRatingView!
    @IBOutlet var tvReview: TextViewWithPlaceholder!
    @IBOutlet var viewTaste: UIView!
    @IBOutlet var viewHolder: UIView!
    @IBOutlet var btnContinue: UIButton!
    
    //MARK:- View Life Cycle --
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        btnContinue.setTitle(BTN_Continue, for: .normal)
    }
    

    //MARK:- Button Action Method --
    @IBAction func btnClose(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSkip(_ sender: Any)
    {
        //viewTime.isHidden = true
        btnSkip.isHidden = true
        SetUpData()
    }
    
    @IBAction func btnContinue(_ sender: Any)
    {
        if btnContinue.titleLabel?.text == BTN_Continue
        {
            SetUpData()
        }
        else
        {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    //MARK:- Set Up Data for add review
    func SetUpData()
    {
        tvReview.text = ""
        tvReview.placeholderText = Add_text
        btnContinue.setTitle(BTN_Submit, for: .normal)
        imgMainImage.image = #imageLiteral(resourceName: "Food image02")
        imgSecondryImage.image = #imageLiteral(resourceName: "Server image")
        viewHolder.slideInFromRight()
        viewTaste.isHidden = false
        viewReviewTaste.rating = 0
        viewReviewPackaging.rating = 0
        viewReviewPrice.rating = 0
        lblPackaging.text = LBL_Packaging
        lblPrice.text = LBL_Price
    }
   

}
